﻿using System;
using SQLite;
using System.IO;
using System.Collections.Generic;

namespace md.onecode.app.litra
{

	public class SQLite_iOS 
	{


		private SQLiteConnection _connection;

		public SQLite_iOS(SQLiteConnection connection) { 
		
		     _connection = connection;
			_connection.CreateTable<zl_android>();
			_connection.CreateTable<zl_admin_users>();
			_connection.CreateTable<zl_clients>();
			_connection.CreateTable<zl_depozit>();
			_connection.CreateTable<zl_orders>();
			_connection.CreateTable<zl_orders_detail>();
			_connection.CreateTable<zl_product>();
			_connection.CreateTable<zl_shipping>();
		}





		public void ReplaceAdminUser(List<zl_admin_users> zl)
		{
			for (int i = 0; i < zl.Count; i++)
			{

				_connection.InsertOrReplace(zl[i]);

			}
			_connection.Commit();

		}

		public List<zl_admin_users> GetAdminUser()
		{
			var temp = _connection.Query<zl_admin_users>("SELECT * FROM [zl_admin_users]");
			return temp;

		}



		public void ReplaceDate(List<zl_android> zl)
		{
			for (int i = 0; i < zl.Count; i++) {

				var temp= _connection.InsertOrReplace(zl[i]);
			}
			_connection.Commit();

		}

		public List<zl_android> GetDate()
		{
			var temp=_connection.Query<zl_android>("SELECT * FROM [zl_android]");
			return temp;

		}




		public void ReplaceClients(List<zl_clients> zl)
		{
			for (int i = 0; i < zl.Count; i++)
			{
				_connection.InsertOrReplace(zl[i]);
			}
			_connection.Commit();

		}

		public List<zl_clients> GetClients()
		{
			var temp = _connection.Query<zl_clients>("SELECT * FROM [zl_clients]");
			return temp;
		}

		public List<zl_clients> GetCompleteClients() { 
        var temp = _connection.Query<zl_clients>("SELECT * FROM [zl_clients]");

			for (int i = 0; i < temp.Count; i++) {


                List<zl_shipping> list = GetShippingByClientId(temp[i].id);

                if (list.Count != 0)
                {
                    temp[i].SetShippingAdress(list);

                }else{

                    temp.Remove(temp[i]);
                }
			
			}

			return temp;

		
		}


		public zl_clients GetClientById(int id)
		{

			//Console.WriteLine("id client "+id);
			var temp = _connection.Table<zl_clients>().Where(v => v.id.Equals(id));

			if (temp.Count() == 0)
				return null;

			return temp.FirstOrDefault();
		}

		public List<zl_clients> GetCompleteClientsByUserId(int id_users)
		{

	
			var temp = _connection.Query<zl_clients>("SELECT * FROM [zl_clients] WHERE id_users = ?", id_users);

			for (int i = 0; i < temp.Count; i++)
			{




                List<zl_shipping> list = GetShippingByClientId(temp[i].id);

                if (list.Count != 0)
                {
                    temp[i].SetShippingAdress(list);

                }
                else
                {

                    temp.Remove(temp[i]);
                }

			}

			return temp;
		}


		public void ReplaceDepozit(List<zl_depozit> zl)
		{
			for (int i = 0; i < zl.Count; i++)
			{
				_connection.InsertOrReplace(zl[i]);
			}
			_connection.Commit();

		}


	

        public void ReplaceDepozitUnits(string id,string units){

            _connection.Execute("UPDATE zl_depozit  SET number_units = " + units + " WHERE id = " + id);
        }


		public List<zl_depozit> GetDepozits()
		{
			var temp = _connection.Query<zl_depozit>("SELECT * FROM [zl_depozit]");
			return temp;
		}


		public List<zl_depozit> GetCompleteDepozits() { 
            var temp = _connection.Query<zl_depozit>("SELECT * FROM [zl_depozit] WHERE number_units != 0");


			for (int i = 0; i < temp.Count; i++) {


				temp[i].SetProduct(GetProducts(temp[i].id_product));
			
			}


			return temp;
		
		
		}


		public zl_depozit GetDepozitById(int id)
		{
			var temp = _connection.Table<zl_depozit>().Where(v => v.id.Equals(id));


			if (temp.Count() == 0)
				return null;

			zl_product zls = GetProducts(temp.FirstOrDefault().id_product);
			//Console.WriteLine("Product A2 "+zls.name);

			zl_depozit zld = temp.FirstOrDefault();
			zld.SetProduct(zls);




			return zld;
		}

		public void ReplaceOrders(List<zl_orders> zl)
		{
			for (int i = 0; i < zl.Count; i++)
			{
				_connection.InsertOrReplace(zl[i]);
			}
			_connection.Commit();

		}


		public void ReplaceOrder(zl_orders zl) {

			_connection.InsertOrReplace(zl);
		
		


		}


        public int getOrderCountByStatus(int status){
            var temp = _connection.Query<zl_orders>("SELECT * FROM [zl_orders] WHERE status = "+status);
            return temp.Count;
        }


        public int getOrderCountByStatus(int status,int userId){
            var temp = _connection.Query<zl_orders>("SELECT * FROM [zl_orders] WHERE status = " + status+" AND id_client = "+userId);
            return temp.Count;
        }

		public List<zl_orders> GetOrders()
		{
			var temp = _connection.Query<zl_orders>("SELECT * FROM [zl_orders]");
			return temp;
		}


		public List<zl_orders> GetCompleteOrders() { 
		
            var temp = _connection.Query<zl_orders>("SELECT * FROM [zl_orders] ORDER BY status ASC , date DESC");
			Console.WriteLine("Initial size Order "+temp.Count);
            int removeCount = 0;

			List<zl_orders> aliveOrders=new List<zl_orders>();
			for (int i = 0; i < temp.Count; i++) {

				bool alive = true;
				zl_orders zl = temp[i];
                zl_shipping zlShipping = GetShippingById(zl.id_shipping);
                zl_clients zlClients = GetClientById(zl.id_client);


                List<zl_orders_detail> zld = GetOrdersDetailById(zl.id.ToString());

				if (zld.Count == 0)
				{
                    Console.WriteLine("Order id at " + zl.id + " detail size is 0");
					alive = false;

				}
				
				if (zlShipping == null) {
					alive = false;
                    Console.WriteLine("Order id at " + zl.id + " ZlShipping Is Null | id Shipping "+zl.id_shipping);
				}
				if (zlClients == null)
				{
                    Console.WriteLine("Order id at " + zl.id + " ZlCLient Is Null | id Client "+zl.id_client);
					alive = false;
				}

				if (alive)
				{
				    zl.SetShippings(zlShipping);
					zl.SetClient(zlClients);
					zl.SetOrdersDetail(zld);
					aliveOrders.Add(zl);
				}

				}



            Console.WriteLine("AFter Order Size is "+aliveOrders.Count);        
			return aliveOrders;

		
		}


        public List<zl_orders> GetCompleteOrdersLimit(int at)
        {

            var temp1 = _connection.Query<zl_orders>("SELECT * FROM [zl_orders] ORDER BY status ASC , date DESC");
            var temp = temp1.GetRange(at, 50);
            Console.WriteLine("Initial size Order " + temp.Count);
            int removeCount = 0;

            List<zl_orders> aliveOrders = new List<zl_orders>();
            for (int i = 0; i < temp.Count; i++)
            {

                bool alive = true;
                zl_orders zl = temp[i];


                zl_shipping zlShipping = GetShippingById(zl.id_shipping);
                zl_clients zlClients = GetClientById(zl.id_client);


                List<zl_orders_detail> zld = GetOrdersDetailById(zl.id.ToString());

                if (zld.Count == 0)
                {
                    Console.WriteLine("Order id at " + zl.id + " detail size is 0");
                    alive = false;

                }

                if (zlShipping == null)
                {
                    alive = false;
                    Console.WriteLine("Order id at " + zl.id + " ZlShipping Is Null | id Shipping " + zl.id_shipping);
                }
                if (zlClients == null)
                {
                    Console.WriteLine("Order id at " + zl.id + " ZlCLient Is Null | id Client " + zl.id_client);
                    alive = false;
                }

                if (alive)
                {
                    zl.SetShippings(zlShipping);
                    zl.SetClient(zlClients);
                    zl.SetOrdersDetail(zld);
                    aliveOrders.Add(zl);
                }

            }



            Console.WriteLine("AFter Order Size is " + aliveOrders.Count);
            return aliveOrders;


        }

		public List<zl_orders> GetCompleteOrdersByStatusId(int id)
		{

			var temp = _connection.Query<zl_orders>("SELECT * FROM [zl_orders] WHERE status= ?",id);
			Console.WriteLine("Initial size Order " + temp.Count);
			for (int i = 0; i < temp.Count; i++)
			{

				bool alive = true;

                

				zl_shipping zlShipping = GetShippingById(temp[i].id_shipping);
				zl_clients zlClients = GetClientById(temp[i].id_client);


				List<zl_orders_detail> zl = GetOrdersDetailById(temp[i].id.ToString());

				if (zl.Count == 0)
				{
					Console.WriteLine("Order id at " + temp[i].id + " detail size is 0");
					alive = false;

				}
				if (zlShipping == null)
				{
					alive = false;
					Console.WriteLine("Order id at " + temp[i].id + " ZlShipping Is Null | id Shipping " + temp[i].id_shipping);
				}
				if (zlClients == null)
				{
					Console.WriteLine("Order id at " + temp[i].id + " ZlCLient Is Null | id Client " + temp[i].id_client);
					alive = false;
				}

				if (alive)
				{
					temp[i].SetShippings(zlShipping);
					temp[i].SetClient(zlClients);
					temp[i].SetOrdersDetail(zl);

				}
				else
				{

					temp.RemoveAt(i);

				}

			}


			return temp;


		}



		public zl_orders GetCompleteOrders(int id)
		{



			   var temp = _connection.Table<zl_orders>().Where(v => v.id.Equals(id));
			   zl_orders zl = temp.FirstOrDefault();
			    
			    zl.SetShippings(GetShippingById(zl.id_shipping));
			    zl_clients tempClient = GetClientById(zl.id_client);

			if (tempClient == null)
				return null;

				   tempClient.SetShippingAdress(GetShippingByClientId(tempClient.id));
				zl.SetClient(tempClient);
				List<zl_orders_detail> zls = GetOrdersDetailById(zl.id.ToString());
				zl.SetOrdersDetail(zls);

				



			return zl;


		}

		public void ReplaceOrdersDetail(List<zl_orders_detail> zlnew)
		{

		
			for (int i = 0; i < zlnew.Count; i++)
			{
				_connection.InsertOrReplace(zlnew[i]);
			}
			_connection.Commit();

		}


		public void ReplaceOrdersDetail(List<zl_orders_detail> zlnew, int idOrder)
		{

			List<zl_orders_detail> DetailToDelete = GetOrdersDetailById("" + idOrder);
			for (int i = 0; i < DetailToDelete.Count; i++)
			{
				_connection.Delete(DetailToDelete[i]);
			}
			_connection.Commit();


			for (int i = 0; i < zlnew.Count; i++)
			{
				_connection.InsertOrReplace(zlnew[i]);
			}
			_connection.Commit();

		}

		public List<zl_orders_detail> GetOrdersDetail()
		{
			var temp = _connection.Query<zl_orders_detail>("SELECT * FROM [zl_orders_detail]");
			return temp;
		}
		public List<zl_orders_detail>GetOrdersDetailById(string id)
		{

			var temp = _connection.Query<zl_orders_detail>("SELECT * FROM [zl_orders_detail] WHERE id_order= ? ",id);
			List<zl_orders_detail> aliveDetail = new List<zl_orders_detail>();

			for (int i = 0; i < temp.Count; i++)
			{

				zl_orders_detail zld = temp[i];

				zl_depozit zls = GetDepozitById(temp[i].id_deposit);
				if (zls == null)
				{
					temp.Remove(temp[i]);
				}
				else
				{
					zld.SetDepozit(zls);
					aliveDetail.Add(zld);
				}
			}
			return aliveDetail;
		}



		public void ReplaceProducts(List<zl_product> zl)
		{
			for (int i = 0; i < zl.Count; i++)
			{
				_connection.InsertOrReplace(zl[i]);
			}
			_connection.Commit();

		}

		public List<zl_product> GetProducts()
		{
			var temp = _connection.Query<zl_product>("SELECT * FROM [zl_product]");
			return temp;
		}
		public zl_product GetProducts(int id)
		{
			var temp = _connection.Table<zl_product>().Where(v => v.id.Equals(id));
		
			   zl_product zl = temp.FirstOrDefault();
			return zl;
		}




		public void ReplaceShipping(List<zl_shipping> zl)
		{
			for (int i = 0; i < zl.Count; i++)
			{
				_connection.InsertOrReplace(zl[i]);
			}
			_connection.Commit();

		}

		public List<zl_shipping> GetShipping()
		{
			var temp = _connection.Query<zl_shipping>("SELECT * FROM [zl_shipping]");
			return temp;
		}



		public zl_shipping GetShippingById(int id) {
			var temp =_connection.Table<zl_shipping>().Where(v => v.id.Equals(id));
			return temp.FirstOrDefault();
		}



		public List<zl_shipping> GetShippingByClientId(int id_client) { 
		
			var temp=_connection.Query<zl_shipping>("SELECT * FROM [zl_shipping] WHERE id_client= ? ", id_client);

			return temp;


		}




		public void DeleteOrder(zl_orders zl) {

			_connection.Delete(zl);
		
		}








	}

}


