﻿using System;
using System.Drawing;
using CoreGraphics;
using Foundation;
using UIKit;

namespace md.onecode.app.litra
{
	public partial class EditOrderDetailCustomCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("EditOrderDetailCustomCell");
		public static readonly UINib Nib;
		public int pos;
		public zl_orders_detail Model { get; set ;  }
		public event EventHandler DeleteButtonPressed;
		public event EventHandler ProductSelectPressed;
		public event EventHandler ExpireDatePressed;
		public event EventHandler CountPressend;
		public int DepozitId = 0;
		public addOrderViewController Context;
		UILabel KeyBoardLabel;
		static EditOrderDetailCustomCell()
		{
			Nib = UINib.FromName("EditOrderDetailCustomCell", NSBundle.MainBundle);
		}

		protected EditOrderDetailCustomCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}
		public static EditOrderDetailCustomCell Create()
		{
			return (EditOrderDetailCustomCell)Nib.Instantiate(null, null)[0];
		}


		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			UITapGestureRecognizer ProductSelect = new UITapGestureRecognizer(() =>
			{
				
				OnProductSelectPressed();
				// Do something in here
			});

			UITapGestureRecognizer ExpireDateSelect = new UITapGestureRecognizer(() =>
		    {

				OnExpireDatePressed();
				// Do something in here
			});
			UITapGestureRecognizer CountSelect = new UITapGestureRecognizer(() =>
		    {

				OnCountPressed();
			// Do something in here
		    });

			ProductNameLabel.UserInteractionEnabled=true;
			ProductNameLabel.AddGestureRecognizer(ProductSelect);

			DateLabel.UserInteractionEnabled = true;
			DateLabel.AddGestureRecognizer(ExpireDateSelect);

			CountLabel.UserInteractionEnabled = true;
			CountLabel.AddGestureRecognizer(CountSelect);

			PriceTextField.ShouldEndEditing+= PriceTextField_ShouldEndEditing;;

			UIToolbar toolbar = new UIToolbar();
			toolbar.BarStyle = UIBarStyle.Default;
			toolbar.Translucent = true;
			toolbar.SizeToFit();
			KeyBoardLabel = new UILabel(new CGRect(0, 0, 100, 70));

			KeyBoardLabel.Text = "21321";
			// Create a 'done' button for the toolbar and add it to the toolbar
			UIBarButtonItem doneButton = new UIBarButtonItem("Done", UIBarButtonItemStyle.Done,
				(s, e) =>
				{
				this.PriceTextField.ResignFirstResponder();
					
				});
	

			toolbar.SetItems(new UIBarButtonItem[] { new UIBarButtonItem(KeyBoardLabel),new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace) { Width = 50 }, doneButton }, true);
			this.PriceTextField.InputAccessoryView = toolbar;
			PriceTextField.EditingChanged += (sender, e) => ChangeValueOnPriceField();
			PriceTextField.ShouldBeginEditing+= PriceTextField_ShouldBeginEditing;
			CellPosLabel.Text = (pos + 1).ToString();
			UpdateCell();

		}

		private void OnDeleteButtonPressed()
		{
			if (DeleteButtonPressed != null)
			{
				DeleteButtonPressed(this, EventArgs.Empty);
			}
		}
		public void OnExpireDatePressed() {

			if (ExpireDatePressed != null) {

				ExpireDatePressed(this, EventArgs.Empty);
			
			
			}
		
		
		
		}


		public void AddBarButtonText(object sender, EventArgs e)
		{
			var barButtonItem = sender as UIBarButtonItem;
			if (barButtonItem != null)
			{
				this.PriceTextField.Text += barButtonItem.Title;
				UIDevice.CurrentDevice.PlayInputClick();
			}

		}

		private void OnProductSelectPressed() {
			
			if (ProductSelectPressed != null)
			{
				ProductSelectPressed(this, EventArgs.Empty);
			}
		
		}

		private void OnCountPressed()
		{

			if (CountPressend != null)
			{
				CountPressend(this, EventArgs.Empty);
			}

		}


		partial void DeleteButton_TouchUpInside(UIButton sender)
		{
			OnDeleteButtonPressed();

			Console.WriteLine("Delete");
		}


		public void UpdateCell() {
			CountLabel.Text = Model.units.ToString();

			if (Model.GetDepozit() != null)
			{
				ProductNameLabel.Text = Model.GetDepozit().GetProduct().name;
                DateLabel.Text = Model.GetDepozit().expiration_date + " " + Model.GetDepozit().GetVolume() + " | " + Model.GetDepozit().number_units;

				UnitsLabel.Text = "Unitati / " + Model.GetDepozit().number_units + "";
			
				if (Model.new_price == 0)
				{
					double price = Model.GetDepozit().price;

					PriceTextField.Text = Convert.ToString(price);
				}
				else {

					PriceTextField.Text = Convert.ToString(Model.new_price);
				

				}



			}else{



				ProductNameLabel.Text = "Select Product";
				DateLabel.Text = "Select Date";
				CountLabel.Text = "0";
				PriceTextField.Text = "0";
				UnitsLabel.Text = "Unitati";
			}

		
		}


		public UIView GetEditViw() {


			return PriceTextField;
		}


		bool PriceTextField_ShouldBeginEditing(UITextField textField)
		{
			Console.WriteLine("PriceTextField_ShouldBeginEditing");
			KeyBoardLabel.Text = PriceTextField.Text;

			return true;
		}

		void ChangeValueOnPriceField()
		{
			Console.WriteLine("PriceTextField_ValueChanged");
			KeyBoardLabel.Text = PriceTextField.Text;

	

		}

		bool PriceTextField_ShouldEndEditing(UITextField textField)
		{

			Console.WriteLine("PriceTextField_ShouldEndEditing");
			double price = Model.units * Model.GetDepozit().price;
			double newprice=0.0;
			try
			{
				
				newprice = Convert.ToDouble(PriceTextField.Text);
			}
			catch (Exception e){


			}
			if (price != newprice)
			{

				Model.new_price = newprice;

			}
			PriceTextField.ResignFirstResponder();
			return true;
		}
	}

}
