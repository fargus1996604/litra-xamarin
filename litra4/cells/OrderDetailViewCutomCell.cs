﻿using System;

using Foundation;
using UIKit;

namespace md.onecode.app.litra
{
	public partial class OrderDetailViewCutomCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString("OrderDetailViewCutomCell");
		public static readonly UINib Nib;
		public zl_orders_detail Model { get; set; }

		public int pos;
		static OrderDetailViewCutomCell()
		{
			Nib = UINib.FromName("OrderDetailViewCutomCell", NSBundle.MainBundle);
		}

		protected OrderDetailViewCutomCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}
		public static OrderDetailViewCutomCell Create()
		{
			return (OrderDetailViewCutomCell)Nib.Instantiate(null, null)[0];
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();


			CellPos.Text = (pos + 1).ToString();
			ProductName.Text = Model.GetDepozit().GetProduct().name;
            DateLabel.Text = Model.GetDepozit().expiration_date + " " + Model.GetDepozit().GetVolume()+" | "+Model.GetDepozit().number_units;
			CountLabel.Text = Model.units.ToString();
			if (Model.new_price == 0)
			{
				double price = Model.GetDepozit().price;

				PriceLabel.Text = Convert.ToString(price);
			}
			else {

				PriceLabel.Text = Convert.ToString(Model.new_price);


			}
		}

	}
}
