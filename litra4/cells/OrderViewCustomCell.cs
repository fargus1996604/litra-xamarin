﻿using System;

using Foundation;
using UIKit;

namespace md.onecode.app.litra
{
	public partial class OrderViewCustomCell : UITableViewCell
	{

		public zl_orders Model { get; set; }

		public static readonly NSString Key = new NSString("OrderViewCustomCell");
		public static readonly UINib Nib;
		public int pos;
		public event EventHandler LongPressEvent;
		static OrderViewCustomCell()
		{
			Nib = UINib.FromName("OrderViewCustomCell", NSBundle.MainBundle);

		}

		protected OrderViewCustomCell(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
			var longPressGesture = new UILongPressGestureRecognizer(LongPressMethod);
			AddGestureRecognizer(longPressGesture);
		}
		public static OrderViewCustomCell Create()
		{
			return (OrderViewCustomCell)Nib.Instantiate(null, null)[0];
		}

		void LongPressMethod(UILongPressGestureRecognizer gestureRecognizer)
		{
			if (gestureRecognizer.State == UIGestureRecognizerState.Began)
			{

				OnLongPress();
			

			}
		}



		public override void LayoutSubviews()
		{
			base.LayoutSubviews();



			this.ClientNameLabel.Text = Model.GetClient().name;
			this.ShippingAdressLabel.Text = Model.GetShipping().shipping_adress;
			this.DateLabel.Text = Model.GetDate();
			this.PriceLabel.Text = Model.GetPrice();



			switch (Model.status) {

				case 0: this.MiniStatusImage.Image = UIImage.FromFile("images/ministatus0.png"); break;
				case 1: this.MiniStatusImage.Image = UIImage.FromFile("images/ministatus1.png"); break;
				case 2: this.MiniStatusImage.Image = UIImage.FromFile("images/ministatus2.png"); break;
					
			
			}
		
		}
		private void OnLongPress()
		{
			
			if (LongPressEvent != null)
			{
				Console.WriteLine("Long pres from cell");
				LongPressEvent(pos, EventArgs.Empty);
			}
		}
	}
}
