﻿using System;
using System.Collections.Generic;
using UIKit;

namespace md.onecode.app.litra
{
	public class ShippingPickerModel : UIPickerViewModel
	{
		private readonly List<zl_shipping> values;

		public event EventHandler PickerChanged;

		public int selectedCategoryID = 0;

		public ShippingPickerModel(List<zl_shipping> collection)
		{
			this.values = collection;
		}

		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
			return values.Count;
		}


		public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
			return values[(int)row].shipping_adress;
		}

		public override nint GetComponentCount(UIPickerView pickerView)
		{

			return 1;
		}
	}

}
