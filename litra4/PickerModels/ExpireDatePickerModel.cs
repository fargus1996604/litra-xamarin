﻿using System;
using System.Collections.Generic;
using UIKit;

namespace md.onecode.app.litra
{

	public class ExpireDatePickerModel : UIPickerViewModel
	{
		private readonly List<zl_depozit> values;

		public event EventHandler PickerChanged;

		public int selectedCategoryID = 0;

		public ExpireDatePickerModel(List<zl_depozit> collection)
		{
			this.values = collection;
		}

		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
			return values.Count;
		}


		public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
            return values[(int)row].expiration_date + " "+values[(int)row].GetVolume()+" | " + values[(int)row].number_units;
;
		}

		public override nint GetComponentCount(UIPickerView pickerView)
		{

			return 1;
		}
	}
}
