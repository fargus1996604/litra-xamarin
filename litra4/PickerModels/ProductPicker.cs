﻿using System;
using System.Collections.Generic;
using UIKit;

namespace md.onecode.app.litra
{
	public class ProductPicker : UIPickerViewModel
	{
		private readonly List<OrderDetailProductSelectModel> values;

		public event EventHandler PickerChanged;

		public int selectedCategoryID = 0;

		public ProductPicker(List<OrderDetailProductSelectModel> collection)
		{
			this.values = collection;
		}

		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
			return values.Count;
		}


		public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
			return values[(int)row].zlProduct.name;
		}

		public override nint GetComponentCount(UIPickerView pickerView)
		{

			return 1;
		}
	}
}
