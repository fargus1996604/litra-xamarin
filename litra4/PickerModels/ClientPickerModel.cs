﻿using System;
using System.Collections.Generic;
using UIKit;

namespace md.onecode.app.litra
{
	public class ClientPickerModel : UIPickerViewModel
	{
		private readonly List<zl_clients> values;

		public event EventHandler PickerChanged;

		public int selectedCategoryID = 0;

		public ClientPickerModel(List<zl_clients> collection)
		{
			this.values = collection;
		}

		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
			return values.Count;
		}


		public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
			return values[(int)row].name;
		}

		public override nint GetComponentCount(UIPickerView pickerView)
		{

			return 1;
		}
	}

}
