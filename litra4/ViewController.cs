﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Net;

using Newtonsoft.Json;
using System.Security.Cryptography;

using UIKit;

using CoreGraphics;
using ObjCRuntime;
using Foundation;
using System.Drawing;
using System.Text;
using Newtonsoft.Json.Linq;

namespace md.onecode.app.litra
{
	public partial class ViewController : UIViewController
	{

		LoadingOverlay loadPop;

		private UIView activeview;             // Controller that activated the keyboard
		private nfloat scroll_amount = 0.0f;    // amount to scroll 
		private nfloat bottom = 0.0f;           // bottom point
		private float offset = 10.0f;          // extra offset
		private bool moveViewUp = false;           // which direction are we moving

		NSObject keyboardUp;
		NSObject keyboardDown;

		protected ViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.


		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			//this.login_field.Transform = CGAffineTransform.MakeTranslation(0, -88);
			// Keyboard popup
			// Keyboard popup
			keyboardUp = NSNotificationCenter.DefaultCenter.AddObserver
								(UIKeyboard.WillShowNotification, KeyBoardUpNotification);

			// Keyboard Down
			keyboardDown = NSNotificationCenter.DefaultCenter.AddObserver
			(UIKeyboard.WillHideNotification, KeyBoardDownNotification);


			var g = new UITapGestureRecognizer(() => View.EndEditing(true));
			g.CancelsTouchesInView = false; //for iOS5

			View.AddGestureRecognizer(g);

			var plist = NSUserDefaults.StandardUserDefaults;

			string username = plist.StringForKey("username");
			string password = plist.StringForKey("password");


			//Console.WriteLine("user not null" + username + password+" "+plist.BoolForKey("SuccesLogin")+UserSettings.SuccesLogin);

			login_field.Text = username;
			pass_field.Text = password;



			login_field.ShouldReturn += Login_Field_ShouldReturn;
			pass_field.ShouldReturn += Pass_Field_ShouldReturn;

			if (UserSettings.SuccesLogin)
			{
				if (UserSettings.First)
				{
					Login();
				}
			}





		}


		private void KeyBoardUpNotification(NSNotification notification)
		{
			// get the keyboard size
			//CGRect r = UIKeyboard.BoundsFromNotification(notification);
			var val = (NSValue)notification.UserInfo.ValueForKey(UIKeyboard.FrameEndUserInfoKey);
			CGRect r = val.CGRectValue;
			//Console.WriteLine("Keyboard notifi "+r);



			if (this.View.Subviews == null)
			{

				Console.WriteLine("this.View.Subviews null");

			}


			// Find what opened the keyboard
			foreach (UIView view in this.View.Subviews)
			{


				Console.WriteLine("view.IsFirstResponder : " + view.IsFirstResponder);

				if (view.IsFirstResponder)
					activeview = view;
			}
			activeview = pass_field;


			//			Console.WriteLine("View : " + activeview.Frame.Height+" size "+activeview.Frame.Size.Height+" y "+activeview.Frame.Y);
			if (activeview == null)
			{

				Console.WriteLine("activeview null");

			}





			// Bottom of the controller = initial position + height + offset      
			bottom = ((activeview.Frame.Y + activeview.Frame.Height + offset));
			//	Console.WriteLine("Botton : "+bottom);

			// Calculate how far we need to scroll
			scroll_amount = ((r.Height - (View.Frame.Size.Height - bottom)));
			//Console.WriteLine("scroll_amount not  : " + scroll_amount);
			// Perform the scrolling
			if (scroll_amount > 0)
			{
				moveViewUp = true;
				ScrollTheView(moveViewUp);
			}
			else {
				moveViewUp = false;
			}


		}

		private void KeyBoardDownNotification(NSNotification notification)
		{
			if (moveViewUp) { ScrollTheView(false); }
		}


		private void ScrollTheView(bool move)
		{

			// scroll the view up or down
			UIView.BeginAnimations(string.Empty, System.IntPtr.Zero);
			UIView.SetAnimationDuration(0.3);

			CGRect frame = View.Frame;
			frame.Y = 0;
			//Console.WriteLine("scroll_amount  theview: " + scroll_amount);
			if (move)
			{
				frame.Y -= scroll_amount;
			}
			else {
				frame.Y = 0;
				scroll_amount = 0;
			}
			Console.WriteLine("Frame " + frame);
			View.Frame = frame;
			UIView.CommitAnimations();
		}

		partial void Login_btn_TouchUpInside(UIButton sender)
		{
			Login();

		}




		async void Login()
		{
			var bounds = UIScreen.MainScreen.Bounds;

			// show the loading overlay on the UI thread using the correct orientation sizing
			loadPop = new LoadingOverlay(bounds, "Logining...");
			View.Add(loadPop);

			bool GetToken = await request.GetToken();
			if (GetToken)
			{
				HttpResponseModel ResponseModel = await request.Logining(login_field.Text, CalculateMD5Hash(pass_field.Text), this);
				Console.WriteLine("Logining " + ResponseModel.Succes + " " + ResponseModel.Ok);
				if (ResponseModel.Succes)
				{



					var temp = JObject.Parse(ResponseModel.Response);

					if (ResponseModel.Ok == 1)
					{
						UserSettings.SuccesLogin = true;
						UserSettings.Offline = false;
						int type = Int32.Parse(temp["isadmin"].ToString());
						UserSettings.UserType = type;
						UserSettings.Id = Convert.ToInt32(temp["id"]);

						var plist = NSUserDefaults.StandardUserDefaults;
						plist.SetString(login_field.Text, "username");
						plist.SetString(pass_field.Text, "password");
						plist.Synchronize();

						UserSettings.First = false;

						var controller = this.Storyboard.InstantiateViewController("CheckControllerId") as CheckViewController;

						if (controller != null)
						{


							this.NavigationController.PushViewController(controller, true);
						}





					} else {


						loadPop.Hide();

						UIAlertView alert = new UIAlertView()
						{
							Title = "Error",
							Message = "Datele introduse nu sunt valide"
						};
						alert.AddButton("OK");
						alert.Show();


					}
				}
				else {
					int button = await ShowAlert("Error!", "Nu exista conexiune la internet", "OK", "Work offline");
					if (button != 0)
					{
						UserSettings.Offline = true;
						UserSettings.SuccesLogin = false;
						Console.WriteLine("userType : " + UserSettings.UserType);
						if (UserSettings.UserType != 2)
						{
							var controller = this.Storyboard.InstantiateViewController("panelControllerid") as panelViewController;


							if (controller != null)
							{


								this.NavigationController.PushViewController(controller, true);
							}
						}
						else { 
						
			            	var controller = this.Storyboard.InstantiateViewController("depozitViewControllerId") as DepozitViewController;


							if (controller != null)
							{


								this.NavigationController.PushViewController(controller, true);
							}

						
						
						
						}



					}
				}
			}
			else {


				int button = await ShowAlert("Error!", "Nu exista conexiune la internet", "OK", "Work offline");
				if (button != 0)
				{
					UserSettings.Offline = true;
					UserSettings.SuccesLogin = false;

					if (UserSettings.UserType != 2)
					{
						var controller = this.Storyboard.InstantiateViewController("panelControllerid") as panelViewController;


						if (controller != null)
						{


							this.NavigationController.PushViewController(controller, true);
						}
					}
					else
					{

						var controller = this.Storyboard.InstantiateViewController("depozitViewControllerId") as DepozitViewController;


						if (controller != null)
						{


							this.NavigationController.PushViewController(controller, true);
						}




					}


					}
				loadPop.Hide();

			}
		}




		public static string CalculateMD5Hash(string input)

		{

			// step 1, calculate MD5 hash from input

			MD5 md5 = System.Security.Cryptography.MD5.Create();

			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

			byte[] hash = md5.ComputeHash(inputBytes);

			// step 2, convert byte array to hex string

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < hash.Length; i++)

			{

				sb.Append(hash[i].ToString("x2"));

			}
			Console.WriteLine("hash " + sb.ToString());
			return sb.ToString();

		}

		bool Login_Field_ShouldReturn(UITextField textField)
		{
			login_field.ResignFirstResponder();
			pass_field.BecomeFirstResponder();

			return true;

		}
		bool Pass_Field_ShouldReturn(UITextField textField)
		{

			pass_field.ResignFirstResponder();
			Login();


			return true;
		}
		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
			if (keyboardUp != null && keyboardDown != null)
			{
				NSNotificationCenter.DefaultCenter.RemoveObserver(keyboardUp);
				NSNotificationCenter.DefaultCenter.RemoveObserver(keyboardDown);
			}
		}

	

		public static Task<int> ShowAlert(string title, string message, params string[] buttons)
		{
			var tcs = new TaskCompletionSource<int>();
			var alert = new UIAlertView
			{
				Title = title,
				Message = message
			};
			foreach (var button in buttons)
				alert.AddButton(button);
			alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
			alert.Show();
			return tcs.Task;
		}

	}



}
