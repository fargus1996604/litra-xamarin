﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using System.Drawing;


[assembly: ExportRenderer(typeof(Entry), typeof(md.onecode.app.litra.CustomEntryRenderer))]

namespace md.onecode.app.litra
{
	public class CustomEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			// Check for only Numeric keyboard
			if (this.Element.Keyboard == Keyboard.Numeric)
			{
				this.AddDoneButton();
			}
		}

		/// <summary>
		/// Add toolbar with Done button
		/// </summary>
		protected void AddDoneButton()
		{
			UIToolbar toolbar = new UIToolbar(new RectangleF(0.0f, 0.0f, 50.0f, 44.0f));

			var doneButton = new UIBarButtonItem(UIBarButtonSystemItem.Done, delegate
			{
				this.Control.ResignFirstResponder();
			});

			toolbar.Items = new UIBarButtonItem[] {
				new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace),
				doneButton
			};
			this.Control.InputAccessoryView = toolbar;
		}
	}
}
