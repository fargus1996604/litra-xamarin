// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace md.onecode.app.litra
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView bg_small { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView line { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton login_btn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField login_field { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView login_img { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView logo { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pass_field { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView pass_img { get; set; }

        [Action ("Login_btn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Login_btn_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (bg_small != null) {
                bg_small.Dispose ();
                bg_small = null;
            }

            if (line != null) {
                line.Dispose ();
                line = null;
            }

            if (login_btn != null) {
                login_btn.Dispose ();
                login_btn = null;
            }

            if (login_field != null) {
                login_field.Dispose ();
                login_field = null;
            }

            if (login_img != null) {
                login_img.Dispose ();
                login_img = null;
            }

            if (logo != null) {
                logo.Dispose ();
                logo = null;
            }

            if (pass_field != null) {
                pass_field.Dispose ();
                pass_field = null;
            }

            if (pass_img != null) {
                pass_img.Dispose ();
                pass_img = null;
            }
        }
    }
}