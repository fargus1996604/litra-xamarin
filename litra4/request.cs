﻿

using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UIKit;

namespace md.onecode.app.litra
{
	public class request
	{


		static private HttpClient client;
		public static string token;


		public static async Task<bool> GetToken()
		{

			client = new HttpClient();
			Console.WriteLine("asdasdas");

	
			try
			{

				var response = await client.GetAsync("http://app.litra.md/token");

				var JsonResult = response.Content.ReadAsStringAsync().Result;

				response.EnsureSuccessStatusCode();
				if (response.IsSuccessStatusCode)
				{

					string content = await response.Content.ReadAsStringAsync();

					Console.WriteLine("goooo" + content);
					token = content;
					Console.WriteLine("Token true");

					return true;
				}


			}
			catch (Exception e){

				Console.WriteLine("Token ex "+e.Message);


				return false;

			}
			Console.WriteLine("Token false");

			return false;


		}
		public static async Task<HttpResponseModel> Logining(string username,string password,ViewController view)
		{
			HttpResponseModel ResponseModel = new HttpResponseModel();


			try
			{
				//client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"))
				var formContent = new FormUrlEncodedContent(new[]
				   {
				new KeyValuePair<string, string>("_token", token),
				new KeyValuePair<string, string>("username",username),
				new KeyValuePair<string, string>("password",password)
			   });
				client.Timeout = TimeSpan.FromMilliseconds(5000);
                var response = await client.PostAsync("http://app.litra.md/android/login", formContent);

				var JsonResult = response.Content.ReadAsStringAsync().Result;

				response.EnsureSuccessStatusCode();
				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync();
					var temp = JObject.Parse(content);

					int ok = Int32.Parse(temp["ok"].ToString());

					Console.WriteLine("Logining Ok :"+ok);

					ResponseModel.Succes = true;
					ResponseModel.Ok = ok;
					ResponseModel.Response = content;
					return ResponseModel;



				}

			}
			catch (Exception e){

				Console.WriteLine("Logining error "+e.Message+" username pass "+username+" "+password+" token "+token);
				ResponseModel.Succes = false;
				return ResponseModel;

			}
			ResponseModel.Succes = false;

			return ResponseModel;
		}
		public static async Task<HttpResponseModel> GetTable(string param)
		{
			
			HttpResponseModel ResponseModel = new HttpResponseModel();

			try
			{

				//client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"))
				var formContent = new FormUrlEncodedContent(new[]
			   {
				new KeyValuePair<string, string>("_token", token),
				new KeyValuePair<string, string>("form",param),
			   });

                var response = await client.PostAsync("http://app.litra.md/android/tables", formContent);

				var JsonResult = response.Content.ReadAsStringAsync().Result;

				response.EnsureSuccessStatusCode();
				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync();

					Console.WriteLine("response GetTable" + content);
				



					ResponseModel.Succes = true;
					ResponseModel.Response = content;

					return ResponseModel;

				}

			}
			catch (Exception e){
				
				ResponseModel.Succes = false;
				return ResponseModel;
			}

			ResponseModel.Succes = false;
			return ResponseModel;
		}

		public static async Task<HttpResponseModel> SendOrders(zl_orders zl,int Type)
		{

			HttpResponseModel ResponseModel = new HttpResponseModel();


			try{

			//client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"))
			var formContent = new FormUrlEncodedContent(new[]
			   {
				new KeyValuePair<string, string>("_token", token),
					new KeyValuePair<string, string>("_type",""+Type),
				new KeyValuePair<string, string>("order",zl.GetJson()),
			   });

                var response = await client.PostAsync("http://app.litra.md/android/addOrder", formContent);

			var JsonResult = response.Content.ReadAsStringAsync().Result;

			response.EnsureSuccessStatusCode();
			if (response.IsSuccessStatusCode)
			{
					
				string content = await response.Content.ReadAsStringAsync();

				var temp = JObject.Parse(content);

				    Console.WriteLine(temp["ok"]);

				
					int ok = Int32.Parse(temp["ok"].ToString());

					ResponseModel.Succes = true;
					ResponseModel.Ok = ok;


					if(ok == 1){
						string json = temp["order"].ToString();

						ResponseModel.Response = json;


					}

					return ResponseModel;

				

			}

	     	}catch (Exception e){
				

                Console.WriteLine("SendOrder Error "+e.ToString());
				ResponseModel.Succes = false;
				return ResponseModel;
			}

	         ResponseModel.Succes = false;
			return ResponseModel;
		}


		public static async Task<HttpResponseModel> DeleteOrder(string id)
		{

			HttpResponseModel ResponseModel = new HttpResponseModel();
			try
			{
				//client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"))
				var formContent = new FormUrlEncodedContent(new[]
				   {
				new KeyValuePair<string, string>("_token", token),
				new KeyValuePair<string, string>("id_order",id),
			   });

                var response = await client.PostAsync("http://app.litra.md/android/delOrder", formContent);

				var JsonResult = response.Content.ReadAsStringAsync().Result;

				response.EnsureSuccessStatusCode();
				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync();

				    var temp = JObject.Parse(content);

					Console.WriteLine("response DellOrder" + content);

					int ok = Int32.Parse(temp["ok"].ToString());

					ResponseModel.Ok = ok;
					ResponseModel.Succes = true;
                    ResponseModel.Response = content;


					return ResponseModel;

				}
			}
			catch (Exception e){



				ResponseModel.Succes = false;
				return ResponseModel;

			}


	         ResponseModel.Succes = false;
			return ResponseModel;
		}
		public static async Task<HttpResponseModel> ChangeStatus(string id,string status)
		{

			HttpResponseModel ResponseModel = new HttpResponseModel();
			try
			{
				//client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"))
				var formContent = new FormUrlEncodedContent(new[]
				{
					
				new KeyValuePair<string, string>("_token", token),
				new KeyValuePair<string, string>("id_order",id),
				new KeyValuePair<string, string>("status",status),

			    });

                var response = await client.PostAsync("http://app.litra.md/android/changeStatus", formContent);

				var JsonResult = response.Content.ReadAsStringAsync().Result;

				response.EnsureSuccessStatusCode();
				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync();
					var temp = JObject.Parse(content);

					Console.WriteLine("response DellOrder" + content);
					int ok = Int32.Parse(temp["ok"].ToString());


					ResponseModel.Succes = true;
					ResponseModel.Ok = ok;
					return ResponseModel;

				}


			}
			catch (Exception e){




				ResponseModel.Succes = false;
				return ResponseModel;
			}

	        ResponseModel.Succes = false;
			return ResponseModel;
		}


		public static async Task<string> GetDate()
		{
			Console.WriteLine("asdasdas");
			try
			{
				//client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"))
				var formContent = new FormUrlEncodedContent(new[]
				   {
				new KeyValuePair<string, string>("_token", token),
				new KeyValuePair<string, string>("id","UvVtKgDC75Cochq80yILT8kzqA64fPyXPa1dbbYE"),
			   });

                var response = await client.PostAsync("http://app.litra.md/android/request", formContent);

				var JsonResult = response.Content.ReadAsStringAsync().Result;

				response.EnsureSuccessStatusCode();
				if (response.IsSuccessStatusCode)
				{
					string content = await response.Content.ReadAsStringAsync();

					Console.WriteLine("response GetDte" + content);

					return content;

				}
			}
			catch (Exception e){



				return "";
			}


			return "";
		}


		public static string MD5Encrypt(String input)
		{
			SHA512 shaM = new SHA512Managed();
			// Convert the input string to a byte array and compute the hash.
			byte[] data = shaM.ComputeHash(Encoding.UTF8.GetBytes(input));
			// Create a new Stringbuilder to collect the bytes
			// and create a string.
			StringBuilder sBuilder = new StringBuilder();
			// Loop through each byte of the hashed data 
			// and format each one as a hexadecimal string.
			for (int i = 0; i < data.Length; i++)
			{
				sBuilder.Append(data[i].ToString("x2"));
			}
			// Return the hexadecimal string.
			input = sBuilder.ToString();
			Console.WriteLine("hash "+input);
			return (input);
		}

		public static string CalculateMD5Hash(string input)

		{

			// step 1, calculate MD5 hash from input

			MD5 md5 = System.Security.Cryptography.MD5.Create();

			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);

			byte[] hash = md5.ComputeHash(inputBytes);

			// step 2, convert byte array to hex string

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < hash.Length; i++)

			{

				sb.Append(hash[i].ToString("x2"));

			}
			Console.WriteLine("hash " + sb.ToString());
			return sb.ToString();

		}

	}
}
