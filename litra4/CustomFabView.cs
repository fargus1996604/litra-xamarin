﻿using System;
using UIKit;
#if __UNIFIED__
using CoreAnimation;
using CoreGraphics;
using Foundation;
using UIKit;
#else
using MonoTouch.CoreAnimation;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

using CGRect = System.Drawing.RectangleF;
using CGSize = System.Drawing.SizeF;
using CGPoint = System.Drawing.PointF;
using nfloat = System.Single;
#endif


namespace md.onecode.app.litra
{
	public class CustomFabView : UIControl
	{
		public CustomFabView()
		{
		}


		public static UIBezierPath PathPlus()
		{

			nfloat PI2 = (nfloat)Math.PI / 2f;
			nfloat internalRadiusRatio = 20.0f / 56.0f;
		    var radius = 8.035714f;
			var center = new CGPoint(22.5f, 22.5f);


		var points = new[] {
				CirclePoint(center, radius, PI2 * 0f + 0),
				CirclePoint(center, radius, PI2 * 1f + 0),
				CirclePoint(center, radius, PI2 * 2f + 0),
				CirclePoint(center, radius, PI2 * 3f + 0)
			};
		    var path = new UIBezierPath();
		    // path.MoveTo(points[0]);
			path.AddArc(center, radius, 0f, 360f*PI2, true);

			return path;
		 }


		 public static CGPoint Minus(CGPoint self, CGPoint point)
	     {
		return new CGPoint(self.X - point.X, self.Y - point.Y);
	     }

		public static  CGPoint CirclePoint(CGPoint circleCenter, nfloat radius, nfloat rad)
		{
			var x = circleCenter.X + radius * (nfloat)Math.Cos(rad);
			var y = circleCenter.Y + radius * (nfloat)Math.Sin(rad);
			return new CGPoint(x, y);
		}
	}
}
