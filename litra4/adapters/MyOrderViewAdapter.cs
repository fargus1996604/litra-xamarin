﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace md.onecode.app.litra
{
	public class MyOrderViewAdapter : UITableViewSource
	{


        public readonly List<zl_orders> _orders;
		public int IdView;

		UIViewController context;
        public event EventHandler LongPressEvent;
        public event EventHandler OnLoadMoreEvent;



		public MyOrderViewAdapter(List<zl_orders> detail,UIViewController context,int idview)
		{
			_orders = detail;
			this.context = context;
			this.IdView = idview;
		}
		public MyOrderViewAdapter(List<zl_orders> detail,panelViewController context)
		{
			_orders = detail;
			this.context = context;
			LongPressEvent += context.Adapter_LongPressEvent;
            OnLoadMoreEvent += context.OnLoadDataNeed;
		}


	   

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var order = _orders[indexPath.Row];
           
			var cell = (OrderViewCustomCell)tableView.DequeueReusableCell(OrderViewCustomCell.Key);
			if (cell == null)
			{
				cell = OrderViewCustomCell.Create();
			}

			cell.Model = order;
			cell.pos = indexPath.Row;
			cell.LongPressEvent+= Cell_LongPressEvent;
            if (indexPath.Row == _orders.Count-1 && OnLoadMoreEvent != null){
                OnLoadMoreEvent(null, EventArgs.Empty);
            }
			return cell;
		}





        public void updateArray(List<zl_orders> items)
        {

            Console.WriteLine("items size blea "+_orders.Count+" in "+items.Count);

            _orders.AddRange(items);
            Console.WriteLine("items size blea 2 "+_orders.Count);

        }

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			return 130;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{
			var controller = context.Storyboard.InstantiateViewController("orderDetailViewCotrollerId") as OrderDetailViewController;
			controller.FromView = IdView;
			if (controller != null)
			{
				controller.id = _orders[indexPath.Row].id;
				context.NavigationController.PushViewController(controller, true);
			}
			tableView.DeselectRow(indexPath, true);
		}
		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return _orders.Count();
		}

		void Cell_LongPressEvent(object sender, EventArgs e)
		{


			if (LongPressEvent != null)
			{
				Console.WriteLine("Long pres from adapter not null");
				LongPressEvent(sender, EventArgs.Empty);
			}
			else { 
			Console.WriteLine("Long pres from adapter null");
			
			}

		}
	}
}
