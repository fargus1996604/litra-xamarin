﻿using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;

namespace md.onecode.app.litra
{
	public class MyOrderDetailViewAdapter : UITableViewSource
	{


		private readonly List<zl_orders_detail> _details;

		public MyOrderDetailViewAdapter(List<zl_orders_detail> details)
		{
			_details = details;
		}


	   

		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{
			var detail = _details[indexPath.Row];
			var cell = (OrderDetailViewCutomCell)tableView.DequeueReusableCell(OrderDetailViewCutomCell.Key);
			if (cell == null)
			{
				cell = OrderDetailViewCutomCell.Create();
			}
			cell.Model = detail;
			cell.pos = indexPath.Row;

			return cell;
		}

		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{
			
			return 130;
		}

		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{

			Console.WriteLine("Select");
			
			tableView.DeselectRow(indexPath, true);
		}


		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return _details.Count();
		}
	}
}
