﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Foundation;
using SharpMobileCode.ModalPicker;
using UIKit;

namespace md.onecode.app.litra
{
	public class MyEditOrderDetailViewAdapter : UITableViewSource
	{

		private readonly List<zl_orders_detail> _details;
		private List<zl_depozit> _depozits;
		public List<OrderDetailProductSelectModel> DepozitzModels=new List<OrderDetailProductSelectModel>();

		public UIViewController Context;
		public List<string> CountArray=new List<string>();

		public MyEditOrderDetailViewAdapter(UIViewController context ,List<zl_orders_detail> details,List<zl_depozit> depozits,List<zl_product> zlProducts)
		{
			_details = details;
			_depozits = depozits;
			Context = context;


			for (int i = 0; i < zlProducts.Count; i++) {


				List<zl_depozit> temp = new List<zl_depozit>();
				for (int j = 0; j < _depozits.Count; j++){




					if (zlProducts[i].id == depozits[j].GetProduct().id) {
						temp.Add(_depozits[j]);
					
					
					}

				}
	         		
			
			if (temp.Count > 0)
				{
					DepozitzModels.Add(new OrderDetailProductSelectModel(zlProducts[i], temp));

				}
			}


			for (int i = 0; i < 1000; i++) {

				CountArray.Add(i.ToString());
			
			
			}

		}




		public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
		{

			var detail = _details[indexPath.Row];
			var cell = (EditOrderDetailCustomCell)tableView.DequeueReusableCell(EditOrderDetailCustomCell.Key);
			if (cell == null)
			{
				cell = EditOrderDetailCustomCell.Create();
				cell.DeleteButtonPressed += async delegate {
					int button = await ShowAlert("Avertizare!", "Confirmati stergerea itemului ?", "Da", "Nu");
					if (button == 0) { 
					_details.RemoveAt(indexPath.Row);
						tableView.ReloadData();

					}




				}; 
					cell.ProductSelectPressed += Cell_ProductSelectPressed;
				    cell.ExpireDatePressed+= Cell_ExpireDatePressed;
				cell.CountPressend+= Cell_CountPressend;
			}
			cell.Model = detail;
			cell.pos = indexPath.Row;



			return cell;


		}


		public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		{


			tableView.DeselectRow(indexPath, true);
		}
		public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
		{

			return 130;
		}
		public override nint RowsInSection(UITableView tableview, nint section)
		{
			return _details.Count;
		}

		public static Task<int> ShowAlert(string title, string message, params string[] buttons)
		{
			var tcs = new TaskCompletionSource<int>();
			var alert = new UIAlertView
			{
				Title = title,
				Message = message
			};
			foreach (var button in buttons)
				alert.AddButton(button);
			alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
			alert.Show();
			return tcs.Task;
		}

		async void Cell_ProductSelectPressed(object sender, EventArgs e)
		{

			var zl = (EditOrderDetailCustomCell)sender;

			ProductPicker model = new ProductPicker(DepozitzModels);
			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Product", Context)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;



			modalPicker.OnModalPickerDismissed += (s, ea) =>
		    {


			    int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);
				zl_depozit depozit = DepozitzModels[temp].zlDepozits[0];

				if (zl.Model.GetDepozit() == null)
				{
					zl.Model.SetDepozit(depozit);
				}
				else if (depozit.id != zl.Model.GetDepozit().id){
					zl.Model.SetDepozit(depozit);
					zl.Model.new_price = 0;
				}

			
				zl.UpdateCell();


		    };
			await Context.PresentViewControllerAsync(modalPicker, true);

		}

		async void Cell_ExpireDatePressed(object sender, EventArgs e)
		{


			var zl = (EditOrderDetailCustomCell)sender;
			if (zl.Model.GetDepozit() == null) {

				Cell_ProductSelectPressed(zl, e);
				return;
			
			}
			var Dmodels = GetSelectedProduct(zl.pos);

			ExpireDatePickerModel model = new ExpireDatePickerModel(Dmodels.zlDepozits);
			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Date", Context)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;



			modalPicker.OnModalPickerDismissed += (s, ea) =>
			{


				int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);
				zl_depozit depozit = Dmodels.zlDepozits[temp];

				zl.Model.SetDepozit(depozit);

				if (depozit.id != zl.Model.GetDepozit().id)
				{
					zl.Model.new_price = 0;
				}


				zl.UpdateCell();



			};
			await Context.PresentViewControllerAsync(modalPicker, true);

		}

		async void Cell_CountPressend(object sender, EventArgs e)
		{
			var zl = (EditOrderDetailCustomCell)sender;


			SimplePickerModel model = new SimplePickerModel(CountArray);
			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Date", Context)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;



			modalPicker.OnModalPickerDismissed += (s, ea) =>
			{


				int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);


				zl.Model.units = Convert.ToInt32(CountArray[temp]);
				zl.Model.new_price = 0;
				zl.UpdateCell();



			};
			await Context.PresentViewControllerAsync(modalPicker, true);
		}

		public OrderDetailProductSelectModel GetSelectedProduct(int row) {

			OrderDetailProductSelectModel temp = new OrderDetailProductSelectModel();


			for (int i = 0; i < DepozitzModels.Count; i++) {
				if (DepozitzModels[i].zlProduct.id == _details[row].GetDepozit().GetProduct().id) {


					temp = DepozitzModels[i];
				}
			
			
			
			}




			return temp;
		}


	}
}
