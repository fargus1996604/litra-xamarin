﻿using System;
using Foundation;

namespace md.onecode.app.litra
{
	public class UserSettings
	{
		public static bool First = true;
		public static bool IsAdmin {
			get
			{
				
				return NSUserDefaults.StandardUserDefaults.IntForKey("Type") == 1;
			}

		}

		public static int UserType
		{
			get
			{

				return (int)NSUserDefaults.StandardUserDefaults.IntForKey("Type");
			}
			set
			{

				NSUserDefaults.StandardUserDefaults.SetInt(value, "Type");
			}

		}



		public static bool SuccesLogin { 
			get
			{
				
				return NSUserDefaults.StandardUserDefaults.BoolForKey("SuccesLogin");

			}
			set  
			{

				NSUserDefaults.StandardUserDefaults.SetBool(value, "SuccesLogin");

			
			}
		
		}
		public static int Id
		{
			get
			{
				
				return (int)NSUserDefaults.StandardUserDefaults.IntForKey("id");
			}
			set
			{
				
					NSUserDefaults.StandardUserDefaults.SetInt(value, "id");

			}

		}


		public static bool Offline = false;


	}
}
