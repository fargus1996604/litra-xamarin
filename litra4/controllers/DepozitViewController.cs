using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using SQLite;

namespace md.onecode.app.litra
{
    public partial class DepozitViewController : UIViewController
    {
		private SQLiteConnection _connection;
		SQLite_iOS db;
		public List<zl_orders> zlOrders;

        public DepozitViewController (IntPtr handle) : base (handle)
        {
        }


		public override void ViewDidLoad()
		{
			base.ViewDidLoad();



			string applicationFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CanFindLocation");

			// Create the folder path.
			System.IO.Directory.CreateDirectory(applicationFolderPath);

			string databaseFileName = System.IO.Path.Combine(applicationFolderPath, "CanFindLocation.db");
			_connection = new SQLiteConnection(databaseFileName);
			db = new SQLite_iOS(_connection);

			zlOrders = db.GetCompleteOrdersByStatusId(0);


			TableView.Source = new MyOrderViewAdapter(zlOrders, this,2);

		}



		partial void BackButton_TouchUpInside(UIButton sender)
		{
			UserSettings.SuccesLogin = false;

			var controller = this.Storyboard.InstantiateViewController("loginViewController") as ViewController;

			if (controller != null)
			{
				this.NavigationController.PushViewController(controller, true);
			}

		}






        partial void UIButton56270_TouchUpInside(UIButton sender)
        {


      
            var controller = this.Storyboard.InstantiateViewController("CheckControllerId") as CheckViewController;

            if (controller != null)
            {


                this.NavigationController.PushViewController(controller, true);
            }



        




        }
    }
}