using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using SQLite;
using SharpMobileCode.ModalPicker;
using CoreGraphics;

namespace md.onecode.app.litra
{
    public partial class searchViewController : UIViewController
    {

		private SQLiteConnection _connection;
		SQLite_iOS db;
		public List<zl_orders> zlOrders;
		List<zl_clients> zlClients=new List<zl_clients>();
		List<string> PayType = new List<string> { "Toate","Transfer", "Cash","Cec" };
		List<string> StatusType = new List<string> { "Toate","Inregistrat", "Livrat", "Achitat" };
		zl_clients selectedClient;
		zl_shipping selectedShipping;
		int PaytypeIndex;
		int StatusIndex;
		zl_clients firstClient;
		zl_shipping firstShipping;
		List<zl_orders> temp = new List<zl_orders>();
		string date;

		public searchViewController(IntPtr handle) : base(handle)
		{ 
		}


		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

		

			string applicationFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CanFindLocation");

			// Create the folder path.
			System.IO.Directory.CreateDirectory(applicationFolderPath);

			string databaseFileName = System.IO.Path.Combine(applicationFolderPath, "CanFindLocation.db");
			_connection = new SQLiteConnection(databaseFileName);
			db = new SQLite_iOS(_connection);




		

			firstClient = new zl_clients() { id = 0, name = "Toate" };


			firstShipping = new zl_shipping() {id=0,shipping_adress="Toate" };


			List<zl_clients> tempClients;
			if (UserSettings.IsAdmin)
			{
				tempClients = db.GetCompleteClients();
			}
			else
			{

				tempClients = db.GetCompleteClientsByUserId(UserSettings.Id);

			}


			zlClients.Add(firstClient);
			for (int i = 0; i < tempClients.Count; i++) { zlClients.Add(tempClients[i]); }


			UpdateClientText();
			UpdatePayTypeText();
			UpdateStatusText();

            InvokeOnMainThread(delegate
            {
                zlOrders = db.GetCompleteOrders();
                UpdateOrders();
            });


			TableView.Source = new MyOrderViewAdapter(temp, this,1);

			DateLabel.Text = "none";
    

			
			var g = new UITapGestureRecognizer(() => View.EndEditing(true));
			g.CancelsTouchesInView = false; //for iOS5


			View.AddGestureRecognizer(g);
			SetLabelGeasture();
		}


		public void SetLabelGeasture()
		{
			UITapGestureRecognizer ClientTab = new UITapGestureRecognizer(() => ShowClientPicker());
			ClientNameLabel.UserInteractionEnabled = true;
			ClientNameLabel.AddGestureRecognizer(ClientTab);




			UITapGestureRecognizer ShippingTab = new UITapGestureRecognizer(() => ShowShippingPicker());
			ShippingAdressLabel.UserInteractionEnabled = true;
			ShippingAdressLabel.AddGestureRecognizer(ShippingTab);



			UITapGestureRecognizer PayTypeTab = new UITapGestureRecognizer(() => ShowPayTypePicker());
			PayTypeLabel.UserInteractionEnabled = true;
			PayTypeLabel.AddGestureRecognizer(PayTypeTab);


			UITapGestureRecognizer StatusTypeTab = new UITapGestureRecognizer(() => ShowStatusPicker());
			StatusLabel.UserInteractionEnabled = true;
			StatusLabel.AddGestureRecognizer(StatusTypeTab);


			UITapGestureRecognizer DateTab = new UITapGestureRecognizer(() => ShowDatePicker());
			DateLabel.UserInteractionEnabled = true;
			DateLabel.AddGestureRecognizer(DateTab);


		}


		public void UpdateOrders(){
			Console.WriteLine("Cliskskks" + zlOrders.Count);
			temp = new List<zl_orders>();


			for (int i = 0; i < zlOrders.Count; i++)
			{

				bool alive = true;
				if (selectedClient != null)
				{
					Console.WriteLine("Client not null");
					if (zlOrders[i].GetClient().id != selectedClient.id)
					{
						alive = false;
					}

				}
				if (selectedShipping != null)
				{
					Console.WriteLine("Shipping not null");
					if (zlOrders[i].id_shipping != selectedShipping.id)
					{
						alive = false;
					}

				}
				if (PaytypeIndex != 0)
				{
					Console.WriteLine("PayType not 0");
                    if (zlOrders[i].pay_type != PaytypeIndex - 1)
					{
						alive = false;
					}
				}
				if (StatusIndex != 0)
				{
					Console.WriteLine("Status not 0");
					if (zlOrders[i].status != StatusIndex - 1)
					{
						alive = false;
					}
				}
				if (date != null) { 
				
					if (!zlOrders[i].date.Equals(date))
					{
						alive = false;
					}
				}


				if (alive)
				{
					temp.Add(zlOrders[i]);
                    Console.WriteLine("alive" + temp.Count);

				}


			}


				TableView.Source = new MyOrderViewAdapter(temp, this,1);

				TableView.ReloadData();



		}

		public void UpdateClientText()
		{


			if (selectedClient != null)
			{
				ClientNameLabel.Text = selectedClient.name;
			}
			else { 
				ClientNameLabel.Text = "Toate";
			}
			if (selectedShipping != null)
			{
				ShippingAdressLabel.Text = selectedShipping.shipping_adress;
			}
			else
			{
				ShippingAdressLabel.Text = "Toate";
			}


		}


		public void UpdatePayTypeText()
		{

			PayTypeLabel.Text = PayType[PaytypeIndex];
		}
		public void UpdateStatusText()
		{
			//
			StatusLabel.Text = StatusType[StatusIndex];
			Console.WriteLine(" Update text "+StatusType[StatusIndex]);
		}



		public async void ShowDatePicker()
		{


			var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Select A Date", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.DatePicker.Mode = UIDatePickerMode.Date;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {
			   var dateFormatter = new NSDateFormatter()
			   {
				   DateFormat = "YYYY-MM-dd"
			   };


			
				date = dateFormatter.ToString(modalPicker.DatePicker.Date);
			    DateLabel.Text = GetDate();
				UpdateOrders();
		   };

			await PresentViewControllerAsync(modalPicker, true);

		}
		public String GetDate()
		{


			Console.WriteLine("Date " + date);
			string temp = date;
			char a = '-';
			string[] split = temp.Split(a);
			string ret = "";
			int pos = Int32.Parse(split[1]);
			switch (pos)
			{
				case 1: ret = "ian"; break;
				case 2: ret = "feb"; break;
				case 3: ret = "mar"; break;
				case 4: ret = "apr"; break;
				case 5: ret = "mai"; break;
				case 6: ret = "iun"; break;
				case 7: ret = "iul"; break;
				case 8: ret = "aug"; break;
				case 9: ret = "sep"; break;
				case 10: ret = "oct"; break;
				case 11: ret = "noi"; break;
				case 12: ret = "dec"; break;

			}
			return "" + split[2] + " " + ret + ". " + split[0];
		}

		public async void ShowClientPicker()
		{
			

			ClientPickerModel model = new ClientPickerModel(zlClients);
			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Client", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {


			   int pos = (int)modalPicker.PickerView.SelectedRowInComponent(0);

			   if (pos == 0) { 
					selectedClient = null;
					selectedShipping = null;
				} else {
				   if (selectedClient != null)
				   {
					   if (selectedClient.id != zlClients[pos].id)
					   {

						   selectedClient = zlClients[pos];
						   selectedShipping = null;
					   }

				   }
				   else { 
					
					selectedClient = zlClients[pos];
					}				   


				}
			   UpdateClientText();

			   UpdateOrders();

		   };
			await PresentViewControllerAsync(modalPicker, true);



		}
		public async void ShowShippingPicker()
		{
			List<zl_shipping> temp = new List<zl_shipping>();

			if (selectedClient == null)
			{
				temp.Add(firstShipping);

			}
			else {

				temp.Add(firstShipping);
				for (int i = 0; i < selectedClient.GetShippingAdress().Count;i++) {

					temp.Add(selectedClient.GetShippingAdress()[i]);
				}
			}

			ShippingPickerModel model = new ShippingPickerModel(temp);

			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Shipping", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
	

			modalPicker.PickerView.Model = model;

			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {


			    int pos = (int)modalPicker.PickerView.SelectedRowInComponent(0);
			    if (pos == 0) { selectedShipping = null;} else { selectedShipping = selectedClient.GetShippingAdress()[pos-1]; }


			  UpdateClientText();
			   UpdateOrders();

		   };
			await PresentViewControllerAsync(modalPicker, true);




		}
		public async void ShowPayTypePicker()
		{

			SimplePickerModel model = new SimplePickerModel(PayType);

			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select PayType", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {

			   int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);
			   //	   zlOrders.payType = temp;
			   PaytypeIndex = temp;
			   UpdatePayTypeText();
			   UpdateOrders();


		   };
			await PresentViewControllerAsync(modalPicker, true);



		}


		public async void ShowStatusPicker()
		{

			SimplePickerModel model = new SimplePickerModel(StatusType);

			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Status", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {
			   int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);
				Console.WriteLine("Status pos "+temp);
			   //zlOrders.status = temp;
			   StatusIndex = temp;
			   UpdateStatusText();
			   UpdateOrders();


		   };
			await PresentViewControllerAsync(modalPicker, true);







		}


		CGRect PickerFrameWithSize(CGSize size)
		{
			var screenRect = UIScreen.MainScreen.ApplicationFrame;
			return new CGRect(0f, screenRect.Height - size.Height, size.Width, size.Height);
		}




        partial void BackButton_TouchUpInside(UIButton sender)
		{

			var controller = this.Storyboard.InstantiateViewController("panelControllerid") as panelViewController;

			if (controller != null)
			{


				this.NavigationController.PushViewController(controller, true);
			}
		}

        partial void ClearDateButton_TouchUpInside(UIButton sender)
		{
			DateLabel.Text = "none";
			date = null;
			UpdateOrders();
		}
	}
}