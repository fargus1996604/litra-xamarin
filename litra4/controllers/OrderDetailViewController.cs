using Foundation;
using System;
using UIKit;
using SQLite;

namespace md.onecode.app.litra
{
    public partial class OrderDetailViewController : UIViewController
    {
        public OrderDetailViewController (IntPtr handle) : base (handle)
        {
        }
		public int FromView = 0;

		private SQLiteConnection _connection;
		SQLite_iOS db;
		zl_orders zl;
		public int id;

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();


			string applicationFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CanFindLocation");

			// Create the folder path.
			System.IO.Directory.CreateDirectory(applicationFolderPath);

			string databaseFileName = System.IO.Path.Combine(applicationFolderPath, "CanFindLocation.db");


			_connection = new SQLiteConnection(databaseFileName);
			db = new SQLite_iOS(_connection);


			 zl = db.GetCompleteOrders(id);

			if (zl == null)
			{
				UIButton402_TouchUpInside(new UIButton());
			}

			OrderDetailTableView.Source = new MyOrderDetailViewAdapter(zl.GetOrdersDetail());

			ClientLabel.Text = zl.GetClient().name;
			AdressLabel.Text = zl.GetShipping().shipping_adress;
			PayTypeLabel.Text = zl.GetPayType();
            Console.WriteLine("Status "+zl.pay_type);
			StatusLabel.Text = zl.GetStatusType();
			DescriptionLabel.Text = zl.description;
			DateLabel.Text = zl.GetDate();
			if (FromView != 2 ) {


				SendButton.Hidden = true;
			}







		}

     async partial  void SendButton_TouchUpInside(UIButton sender)
		{
			 ChangeStatusAsync();
		}

		partial void UIButton402_TouchUpInside(UIButton sender)
		{

			if (FromView == 0)
			{

				var controller = this.Storyboard.InstantiateViewController("panelControllerid") as panelViewController;

				if (controller != null)
				{


					this.NavigationController.PushViewController(controller, true);
				}
			}
			else if (FromView == 1)
			{

				var controller = this.Storyboard.InstantiateViewController("searchControllerId") as searchViewController;

				if (controller != null)
				{
					this.NavigationController.PushViewController(controller, true);
				}





			}
			else if (FromView == 2) { 
			
					var controller = this.Storyboard.InstantiateViewController("depozitViewControllerId") as DepozitViewController;


				if (controller != null)
				{


					this.NavigationController.PushViewController(controller, true);
				}



			}
		}

			async void ChangeStatusAsync()
		{
			var bounds = UIScreen.MainScreen.Bounds;

			// show the loading overlay on the UI thread using the correct orientation sizing
		    LoadingOverlay	loadPop = new LoadingOverlay(bounds, "Changing Status");

			View.Add(loadPop);

			int id = zl.id;
			HttpResponseModel content = await request.ChangeStatus(id.ToString(), "1");
			if (content.Succes)
			{


				switch (content.Ok)
				{
					case 0:

						Console.WriteLine("Change Status Error 0");
						UIAlertView alert = new UIAlertView()
						{
							Title = "Error",
							Message = "Aceasta Comanda nu mai exista !!"
						};
						alert.AddButton("OK");

						alert.Show();


					break;



					case 1:


						zl.status = 1;
						db.ReplaceOrder(zl);
					var controller = this.Storyboard.InstantiateViewController("depozitViewControllerId") as DepozitViewController;


						if (controller != null)
						{


							this.NavigationController.PushViewController(controller, true);
						}
						break;


					case 2:
						Console.WriteLine("Change Status Error 3");
						break;


				}
			}
			else
			{
				UIAlertView alert = new UIAlertView()
				{
					Title = "Error",
					Message = "Nu exista conexiune la internet"
				};
				alert.AddButton("OK");

				alert.Show();
			}





			loadPop.Hide();
		}
	}
}