using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using SQLite;
using Newtonsoft.Json.Linq;
using System.Diagnostics.Contracts;
using System.Drawing;
using CoreGraphics;
using AnimatedButtons;
using SharpMobileCode.ModalPicker;
using System.Threading.Tasks;

namespace md.onecode.app.litra
{
    public partial class panelViewController : UIViewController
    {


		private SQLiteConnection _connection;
		SQLite_iOS db;
		List<zl_orders> orders;

		LoadingOverlay loadPop;

		int SelectedActionButton=0;
        MyOrderViewAdapter adapter;

        bool loadingData = false;

		public panelViewController (IntPtr handle) : base (handle)
        {



        }
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			//this.login_field.Transform = CGAffineTransform.MakeTranslation(0, -88);
			// Keyboard popup

			AllOrderButton.TouchUpInside += SwichHeadButtons;
			MyOrderButton.TouchUpInside += SwichHeadButtons;
            string applicationFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CanFindLocation");

			// Create the folder path.
			System.IO.Directory.CreateDirectory(applicationFolderPath);

			string databaseFileName = System.IO.Path.Combine(applicationFolderPath, "CanFindLocation.db");


			_connection = new SQLiteConnection(databaseFileName);
			db = new SQLite_iOS(_connection);

            orders = new List<zl_orders>();


		
		

			OrderTableView.Source = new MyOrderViewAdapter(orders, this);

			adapter = (MyOrderViewAdapter)OrderTableView.Source;
			adapter.LongPressEvent += Adapter_LongPressEvent;

			fab.SimpleFab = true;
			SearchFab.SimpleFab = true;
			SearchFab.ClickEvent += delegate
				{
			    	var controller = this.Storyboard.InstantiateViewController("searchControllerId") as searchViewController;

					if (controller != null)
					{
						this.NavigationController.PushViewController(controller, true);
					}
				};

			if (UserSettings.Offline)
			{

				fab.Enabled = false;
			}
			else {

				fab.ClickEvent += delegate
				{
					var controller = this.Storyboard.InstantiateViewController("addOrderViewControllerId") as addOrderViewController;
					if (controller != null)
					{
						this.NavigationController.PushViewController(controller, true);
					}
				};
			}


		

			if (UserSettings.IsAdmin)
			{
                SwichOrders(1);
				SelectedActionButton = 1;

			}else {

				SwichOrders(0);
				SelectedActionButton = 0;
				AllOrderButton.Enabled = false;
		
			}



	
       
		



		}


        public void OnLoadDataNeed(object sender, EventArgs args)
        {
            if (!loadingData) {
                loadingData = true;
                int last = orders.Count - 1;
                var list = db.GetCompleteOrdersLimit(last);

                InvokeOnMainThread(delegate
                {
                    orders.AddRange(list);
                    OrderTableView.ReloadData();
                    loadingData = false;
                });



            Console.WriteLine("Load More");
           }
        }


		public override void ViewDidLayoutSubviews()
		{
			base.ViewDidLayoutSubviews();


		}

        partial void Update_TouchUpInside(UIButton sender)
        {
            var controller = this.Storyboard.InstantiateViewController("CheckControllerId") as CheckViewController;
            if (controller != null)
            {
                this.NavigationController.PushViewController(controller, true);
            }
        }


       

		public void SwichHeadButtons(object sender, EventArgs ea) {
			UIButton button = (sender as UIButton);
			SelectedActionButton =(int)button.Tag ;
			SwichOrders((int)button.Tag);
		}

		async public  void ChangeOrderStatus(int index)
        {

			if (UserSettings.Offline) {
				ShowAlert("Error", "In regimul offline nu puteti schimba statusul !","ok");
				return;
			}

			PickerModel model = new PickerModel(new List<string>() { "Inregistrat", "Livrat", "Achitat" });


			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Status", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {


				int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);
			   if (orders[index].status != temp)
			   {
				   Console.WriteLine("Selected " + temp);
				   ChangeStatusAsync(index, temp);
			   }

	
		   };

			await PresentViewControllerAsync(modalPicker, true);


		}


	
		async void ChangeStatusAsync(int index,int status)
		{
			var bounds = UIScreen.MainScreen.Bounds;

			// show the loading overlay on the UI thread using the correct orientation sizing
			loadPop = new LoadingOverlay(bounds,"Changing Status");

			View.Add(loadPop);

			int id = orders[index].id;
			HttpResponseModel content = await request.ChangeStatus(id.ToString(),status.ToString());
			if (content.Succes)
			{


				switch (content.Ok)
				{
					case 0:

						Console.WriteLine("Change Status Error 0");
						UIAlertView alert = new UIAlertView()
						{
							Title = "Error",
							Message = "Aceasta Comanda nu mai exista !!"
						};
						alert.AddButton("OK");

						alert.Show();


						break;
					case 1:
						orders[index].status = status;
						db.ReplaceOrder(orders[index]);
						ShowOrders(SelectedActionButton);
						break;
					case 2:
						Console.WriteLine("Change Status Error 3");
					break;


			}
			}else
			{
				UIAlertView alert = new UIAlertView()
				{
					Title = "Error",
					Message = "Nu exista conexiune la internet"
				};
				alert.AddButton("OK");
		
				alert.Show();
			}
			loadPop.Hide();
		}
		CGRect PickerFrameWithSize(CGSize size)
		{
			var screenRect = UIScreen.MainScreen.ApplicationFrame;
			return new CGRect(0f, screenRect.Height - size.Height, size.Width, size.Height);
		}

		async void SwichOrders(int id) {

			switch (id)
			{
				case 0 :

					MyOrderButton.SetBackgroundImage(UIImage.FromFile("images/buttonselected.png"),UIControlState.Normal);
					MyOrderButton.SetAttributedTitle(new NSAttributedString(
						"Comenzile mele", 
						font: UIFont.FromName("HolveticaNeue-Regular", 12.0f), 
						foregroundColor: UIColor.FromRGB(51, 27, 12)), 
					UIControlState.Normal);

					AllOrderButton.SetBackgroundImage(UIImage.FromFile("images/buttonUnselected.png"),UIControlState.Normal);
					AllOrderButton.SetAttributedTitle(new NSAttributedString(
						"Toate Comenzile", 
						font: UIFont.FromName("HolveticaNeue-Regular", 12.0f), 
						foregroundColor: UIColor.FromRGB(255, 255, 255)), 
					UIControlState.Normal);
                    ShowOrders(0);
				break;
				case 1:
					
					MyOrderButton.SetBackgroundImage(UIImage.FromFile("images/buttonUnselected.png"),UIControlState.Normal);
					MyOrderButton.SetAttributedTitle(new NSAttributedString(
						"Comenzile mele",
						font: UIFont.FromName("HolveticaNeue-Regular", 12.0f),
						foregroundColor: UIColor.FromRGB(255, 255, 255)),
					UIControlState.Normal);
					
				    AllOrderButton.SetBackgroundImage(UIImage.FromFile("images/buttonselected.png"),UIControlState.Normal);
					AllOrderButton.SetAttributedTitle(new NSAttributedString("Toate Comenzile", 
					    font: UIFont.FromName("HolveticaNeue-Regular", 12.0f), 
					    foregroundColor: UIColor.FromRGB(51, 27, 12)), 
					UIControlState.Normal);

                    ShowOrders(1);
			
				break;
			}
		 
		}



		async public void DeleteOrder(int index) 
		{


			if (UserSettings.Offline)
			{

				ShowAlert("Error", "In regimul offline nu puteti sterge comanda !", "ok");
				return;




			}


			int button = await ShowAlert("Avertizare!", "Confirmati stergerea ?", "Da", "Nu");
			if (button != 0)
			{
				return;

			}



			var bounds = UIScreen.MainScreen.Bounds;

			// show the loading overlay on the UI thread using the correct orientation sizing
			loadPop = new LoadingOverlay(bounds,"Deleting..");

			View.Add(loadPop);

			int id = orders[index].id;


			HttpResponseModel content = await request.DeleteOrder(id.ToString());

			if (content.Succes)
			{


				switch (content.Ok)
				{
					case 0:

						db.DeleteOrder(orders[index]);
						ShowOrders(SelectedActionButton);
						break;



					case 1:

						db.DeleteOrder(orders[index]);
						ShowOrders(SelectedActionButton);
                        JObject obj = JObject.Parse(content.Response);
                        JArray array = JArray.Parse(obj["deposit"].ToString());


                        for (int i = 0; i < array.Count; i++)
                        {

                            int ids = Int32.Parse(array[i]["id"].ToString());
                            int count = Int32.Parse(array[i]["number_units"].ToString());

                            Console.WriteLine(ids+" "+count);
                            db.ReplaceDepozitUnits(array[i]["id"].ToString(),array[i]["number_units"].ToString());

                        }

                        Console.WriteLine(obj["deposit"].ToString());
						break;



				}
			}else
			{
				UIAlertView alert = new UIAlertView()
				{
					Title = "Error",
					Message = "Nu exista conexiune la internet"
				};
				alert.AddButton("OK");
			
				alert.Show();
			}





			loadPop.Hide();
		
		
		
		}



         void ShowOrders(int type) {
			switch (type)
			{

				case 0:

					List<zl_orders> zl = new List<zl_orders>();
					zl=db.GetCompleteOrdersLimit(0);

					orders.Clear();
					for (int i = 0; i < zl.Count; i++) {
						if (zl[i].id_client == UserSettings.Id)
							orders.Add(zl[i]);
					}
                    RefreshStatus(true);
					break;

				case 1:
					orders.Clear();
                    orders = db.GetCompleteOrdersLimit(0);
                    RefreshStatus(false);
					break;
			}
			OrderTableView.Source = new MyOrderViewAdapter(orders,this);
			OrderTableView.ReloadData();         
		  //  Console.WriteLine("Orders size : " + orders.Count);
		}







        public void RefreshStatus(bool myOrders) {
            if (myOrders)
            {
                statusLabel0.Text = db.getOrderCountByStatus(0,UserSettings.Id).ToString();
                statusLabel1.Text = db.getOrderCountByStatus(1,UserSettings.Id).ToString();
                statusLabel2.Text = db.getOrderCountByStatus(2,UserSettings.Id).ToString();
            }
            else
            {
                statusLabel0.Text = db.getOrderCountByStatus(0).ToString();
                statusLabel1.Text = db.getOrderCountByStatus(1).ToString();
                statusLabel2.Text = db.getOrderCountByStatus(2).ToString();
            }

		}





        partial void UIButton272_TouchUpInside(UIButton sender)
		{

			UserSettings.SuccesLogin = false;

			var controller = this.Storyboard.InstantiateViewController("loginViewController") as ViewController;

			if (controller != null)
			{
				this.NavigationController.PushViewController(controller, true);
			}
			
		}




		public void EditOrder(int id) { 
		

			if (UserSettings.Offline)
			{

				ShowAlert("Error", "In regimul offline nu puteti edita comanda !", "ok");
				return;



			}

			var controller = this.Storyboard.InstantiateViewController("editOrderViewControllerId") as editOrderViewController;

			controller.IdOrder = orders[id].id;

			if (controller != null)
			{
				this.NavigationController.PushViewController(controller, true);
			}
		
		
		}



	

			public static Task<int> ShowAlert(string title, string message, params string[] buttons)
		{
			var tcs = new TaskCompletionSource<int>();
			var alert = new UIAlertView
			{
				Title = title,
				Message = message
			};
			foreach (var button in buttons)
				alert.AddButton(button);
			alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
			alert.Show();
			return tcs.Task;
		}



        public void Adapter_LongPressEvent (object sender, EventArgs e)
		{
				Console.WriteLine("Long pres from panel");
			int pos = (int)sender;

		// Create a new Alert Controller
				UIAlertController actionSheetAlert = UIAlertController.Create(null, null, UIAlertControllerStyle.ActionSheet);

			// Add Actions
			actionSheetAlert.AddAction(UIAlertAction.Create("Editeaza", UIAlertActionStyle.Default, (action) => EditOrder(pos)));

			actionSheetAlert.AddAction(UIAlertAction.Create("Schimba statusu", UIAlertActionStyle.Default, (action) => ChangeOrderStatus(pos)));

			actionSheetAlert.AddAction(UIAlertAction.Create("Sterge", UIAlertActionStyle.Destructive, (action) => DeleteOrder(pos)));

			actionSheetAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, (action) => Console.WriteLine("Cancel button pressed.")));

			// Required for iPad - You must specify a source for the Action Sheet since it is
			// displayed as a popover
			UIPopoverPresentationController presentationPopover = actionSheetAlert.PopoverPresentationController;
			if (presentationPopover != null)
			{
				presentationPopover.SourceView = View;
				presentationPopover.PermittedArrowDirections = UIPopoverArrowDirection.Up;
			}

			// Display the alert
			PresentViewController(actionSheetAlert, true, null);

		}
	}

	public class PickerModel : UIPickerViewModel
	{
		private readonly List<string> values;

		public event EventHandler PickerChanged;

		public int selectedCategoryID = 0;

		public PickerModel(List<string> collection)
		{
			this.values = collection;
		}

		public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
		{
			return values.Count;
		}

	
		 public override string GetTitle(UIPickerView pickerView, nint row, nint component)
		{
			return values[(int)row];
		}

		public override nint GetComponentCount(UIPickerView pickerView)
		{

			return 1;
		}



	}
}
