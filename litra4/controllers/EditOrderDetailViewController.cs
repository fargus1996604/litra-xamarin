using Foundation;
using System;
using UIKit;
using SQLite;
using System.Collections.Generic;
namespace md.onecode.app.litra
{
	public partial class EditOrderDetailViewController : UIViewController
	{
        private SQLiteConnection _connection;
		SQLite_iOS db;


		public int id;
		public EditOrderDetailViewController(IntPtr handle) : base(handle)
		{
		}


		public override void ViewDidLoad()
		{
			base.ViewDidLoad();


			 string applicationFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CanFindLocation");

			// Create the folder path.
			System.IO.Directory.CreateDirectory(applicationFolderPath);

			string databaseFileName = System.IO.Path.Combine(applicationFolderPath, "CanFindLocation.db");


			_connection = new SQLiteConnection(databaseFileName);
			db = new SQLite_iOS(_connection);


			zl_orders zl = db.GetCompleteOrders(id);
			if (zl == null) {
				UIButton402_TouchUpInside(new UIButton());
			}

			OrderDetailTableView.Source = new MyOrderDetailViewAdapter(zl.GetOrdersDetail());



		}

        partial void UIButton402_TouchUpInside(UIButton sender)
		{

			var controller = this.Storyboard.InstantiateViewController("panelControllerid") as panelViewController;

			if (controller != null)
			{


				this.NavigationController.PushViewController(controller, true);
			}



		}
	}
}