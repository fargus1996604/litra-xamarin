using Foundation;
using System;
using UIKit;
using SQLite;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace md.onecode.app.litra
{
    public partial class CheckViewController : UIViewController
    {

		private SQLiteConnection _connection;



		SQLite_iOS db;
		OutModel models;
        public CheckViewController (IntPtr handle) : base (handle)
        {
        }


		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			//this.login_field.Transform = CGAffineTransform.MakeTranslation(0, -88);


			// Keyboard popup
			ActivityIndicator.StartAnimating();



			string applicationFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CanFindLocation");

			// Create the folder path.
			System.IO.Directory.CreateDirectory(applicationFolderPath);

			string databaseFileName = System.IO.Path.Combine(applicationFolderPath, "CanFindLocation.db");


			_connection = new SQLiteConnection(databaseFileName);
			db = new SQLite_iOS(_connection);
			//_connection.DeleteAll<zl_orders>();
			//_connection.DeleteAll<zl_clients>();
			//_connection.DeleteAll<zl_orders_detail>();
			//_connection.DeleteAll<zl_depozit>();
			//_connection.DeleteAll<zl_product>();
			//_connection.DeleteAll<zl_shipping>();
			//_connection.DeleteAll<zl_android>();

			Checking();



		}


		async void Checking() {


			try
			{
				
				string jsons = await request.GetDate();

                Console.WriteLine(jsons);
				List<zl_android> outDate = JsonConvert.DeserializeObject<List<zl_android>>(jsons);
				List<zl_android> inDate = db.GetDate();
				List<zl_android> expireDate = new List<zl_android>();





				if (inDate.Count == 0)
				{
					await Update(outDate);

					db.ReplaceDate(outDate);



				}else{

					if (outDate.Count == inDate.Count)
					{

						for (int i = 0; i < outDate.Count; i++)
						{
                            Console.WriteLine("asdasd"+outDate[i].date_hash);

                            if (outDate[i].date_hash.Equals(inDate[i].date_hash) == false)
							{

								expireDate.Add(inDate[i]);

							}
						}
					}

					if (expireDate.Count > 0)
					{
						await Update(expireDate);
						db.ReplaceDate(outDate);

					}
				}


			


				Console.WriteLine("Orders size " + db.GetOrders().Count + " Detail size " + db.GetOrdersDetail().Count
				                  + " Clients size  " + db.GetClients().Count 
				                  + " depozit size " + db.GetDepozits().Count
				                  + " Product size " + db.GetProducts().Count
				                  + " shipping size " + db.GetShipping().Count);


				if (UserSettings.UserType != 2)
				{
					var controller = this.Storyboard.InstantiateViewController("panelControllerid") as panelViewController;


					if (controller != null)
					{


						this.NavigationController.PushViewController(controller, true);
					}
				}
				else { 
				
				var controller = this.Storyboard.InstantiateViewController("depozitViewControllerId") as DepozitViewController;


					if (controller != null)
					{


						this.NavigationController.PushViewController(controller, true);
					}
				
				}
			}
            catch (Exception e)
			{

                Console.WriteLine("Eoororor "+e.ToString());

				UserSettings.SuccesLogin = false;
			    var controller = this.Storyboard.InstantiateViewController("loginViewController") as ViewController;

				if (controller != null)
				{


					this.NavigationController.PushViewController(controller, true);
				}
			}



		}
		public async Task Update(List<zl_android> zl) { 

			JArray js = new JArray();

			for (int i = 0; i < zl.Count; i++) {

				js.Add(zl[i].table);

			}

			Console.Write("Json Object : "+js);

			HttpResponseModel table = await request.GetTable(js.ToString());
			Console.WriteLine("Succes get table " + table.Succes + " Content " + table.Response);
			if (table.Succes)
			{
				models = JsonConvert.DeserializeObject<OutModel>(table.Response);

				for (int i = 0; i < zl.Count; i++)
				{
					Console.WriteLine("Table tu update: "+zl[i].table);


					UpdateTable(zl[i].table);
				}
			}





		}





		public void UpdateTable(string name) {


			switch (name) {

				case "clients": _connection.DropTable<zl_clients>();_connection.CreateTable<zl_clients>(); db.ReplaceClients(models.clients);  break;
					case "depozit": _connection.DropTable<zl_depozit>(); _connection.CreateTable<zl_depozit>(); db.ReplaceDepozit(models.depozit); break;
					case "orders":_connection.DropTable<zl_orders>(); _connection.CreateTable<zl_orders>(); db.ReplaceOrders(models.orders); break;
					case "orders_detail": _connection.DropTable<zl_orders_detail>(); _connection.CreateTable<zl_orders_detail>(); db.ReplaceOrdersDetail(models.orders_detail); break;
					case "product":_connection.DropTable<zl_product>(); _connection.CreateTable<zl_product>(); db.ReplaceProducts(models.product); break;
					case "shipping": _connection.DropTable<zl_shipping>(); _connection.CreateTable<zl_shipping>(); db.ReplaceShipping(models.shipping); break;
					case "admin_users": _connection.DropTable<zl_admin_users>(); _connection.CreateTable<zl_admin_users>(); db.ReplaceAdminUser(models.admin_users); break;
			
			}
		
		
		}







    }



	public class OutModel {

		public List<zl_clients> clients;
		public List<zl_depozit> depozit;
		public List<zl_orders> orders;
	    public List<zl_orders_detail> orders_detail;
		public List<zl_product> product;
		public List<zl_shipping> shipping;
		public List<zl_admin_users> admin_users;
	
	}



}