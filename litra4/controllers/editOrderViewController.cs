using Foundation;
using System;
using UIKit;
using System.Collections.Generic;
using SQLite;
using CoreGraphics;
using SharpMobileCode.ModalPicker;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace md.onecode.app.litra
{
	public partial class editOrderViewController : UIViewController
	{
		private SQLiteConnection _connection;
		SQLite_iOS db;
		public int IdOrder = 0;
		public zl_orders zlOrders;
		List<zl_clients> zlClients;
		List<string> PayType = new List<string> { "Transfer", "Cash","Cec"};
		List<string> StatusType = new List<string> { "Inregistart", "Livrat", "Achitat" };

		List<zl_depozit> zlDepozit;


		public editOrderViewController(IntPtr handle) : base(handle)
		{



		}



		public override void ViewDidLoad()
		{
			base.ViewDidLoad();


			string applicationFolderPath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "CanFindLocation");

			// Create the folder path.
			System.IO.Directory.CreateDirectory(applicationFolderPath);

			string databaseFileName = System.IO.Path.Combine(applicationFolderPath, "CanFindLocation.db");
			_connection = new SQLiteConnection(databaseFileName);
			db = new SQLite_iOS(_connection);


			zlOrders = db.GetCompleteOrders(IdOrder);


			if (zlOrders == null)
			{
				GoToPanel();
			}


			if (UserSettings.IsAdmin)
			{
				zlClients = db.GetCompleteClients();
			}
			else {

				zlClients = db.GetCompleteClientsByUserId(UserSettings.Id);

			}


			fab.SimpleFab = true;

			Console.WriteLine(" Id order: "+IdOrder+" Scout"+zlOrders.GetClient().GetShippingAdress().Count);
		


			UpdateClientText();
			UpdatePayTypeText();
			UpdateStatusText();
		
			DateLabel.Text = zlOrders.GetDate();
			DescriptionLabel.Text = zlOrders.description;
			DescriptionLabel.ShouldEndEditing += delegate
			{
				zlOrders.description = DescriptionLabel.Text;
				return true;
			};
			zlOrders.description = "none";

			zlDepozit = db.GetCompleteDepozits();
			List<zl_product> zlProducts = db.GetProducts();
			TableView.Source = new MyEditOrderDetailViewAdapter(this, zlOrders.GetOrdersDetail(), zlDepozit, zlProducts);


			fab.ClickEvent += (sender, e) => AddOrderDetail();
			SetLabelGeasture();
			DescriptionLabel.ShouldReturn += ShouldReturn;
			var g = new UITapGestureRecognizer(() => View.EndEditing(true));
			g.CancelsTouchesInView = false; //for iOS5


			View.AddGestureRecognizer(g);

		}
		public void SetLabelGeasture()
		{
			UITapGestureRecognizer ClientTab = new UITapGestureRecognizer(() => ShowClientPicker());
			ClientNameLabel.UserInteractionEnabled = true;
			ClientNameLabel.AddGestureRecognizer(ClientTab);




			UITapGestureRecognizer ShippingTab = new UITapGestureRecognizer(() => ShowShippingPicker());
			ShippingAdressLabel.UserInteractionEnabled = true;
			ShippingAdressLabel.AddGestureRecognizer(ShippingTab);



			UITapGestureRecognizer PayTypeTab = new UITapGestureRecognizer(() => ShowPayTypePicker());
			PayTypeLabel.UserInteractionEnabled = true;
			PayTypeLabel.AddGestureRecognizer(PayTypeTab);


			UITapGestureRecognizer StatusTypeTab = new UITapGestureRecognizer(() => ShowStatusPicker());
			StatusLabel.UserInteractionEnabled = true;
			StatusLabel.AddGestureRecognizer(StatusTypeTab);


			UITapGestureRecognizer DateTab = new UITapGestureRecognizer(() => ShowDatePicker());
			DateLabel.UserInteractionEnabled = true;
			DateLabel.AddGestureRecognizer(DateTab);


		}




		public bool CheckOrder()
		{


			Console.WriteLine("Json OutpUt :" + zlOrders.GetJson());

			if (zlOrders.GetOrdersDetail().Count == 0)
			{


				ShowAlert("Avertizare!", "Detalile sunt obligatorii", new string[] { "OK" });
				return false;
			}

			for (int i = 0; i < zlOrders.GetOrdersDetail().Count; i++)
			{

				if (zlOrders.GetOrdersDetail()[i].units == 0)
				{
					ShowAlert("Avertizare!", "Unitatile sunt obligatorii", new string[] { "OK" });
					TableView.ReloadData();
					TableView.ScrollToRow(NSIndexPath.FromRowSection(i, 0), UITableViewScrollPosition.Bottom, true);
					return false;

				}


				if (zlOrders.GetOrdersDetail()[i].units > zlOrders.GetOrdersDetail()[i].GetDepozit().number_units)
				{

					ShowAlert("Avertizare!", "Ati introdus mai multe unitati decat sunt in depozit", new string[] { "OK" });
					TableView.ReloadData();
					TableView.ScrollToRow(NSIndexPath.FromRowSection(i, 0), UITableViewScrollPosition.Bottom, true);
					return false;

				}

			}





			return true;
		}




		public List<zl_orders_detail> GetCorrectlyDetail()
		{


			List<zl_orders_detail> zl = zlOrders.GetOrdersDetail();

			for (int j = 0; j < zl.Count; j++)
			{
				for (int k = zl.Count - 1; k > j; k--)
				{
					if (zl[j].GetDepozit().id == zl[k].GetDepozit().id)
					{
						if (zl[j].new_price == 0)
						{
							if (zl[k].new_price > 0)
							{
								zl[j].new_price = (zl[j].units * zl[j].GetDepozit().price + zl[k].new_price);
							}
						}
						else {
							if (zl[k].new_price > 0)
							{
								zl[j].new_price = (zl[j].new_price + zl[k].new_price);
							}
							else {
								zl[j].new_price = (zl[j].new_price + (zl[k].units * zl[k].GetDepozit().price));
							}
						}
						zl[j].units = (zl[j].units + zl[k].units);
						zl.RemoveAt(k);
					}
				}
			}

			return zl;


		}

		public void UpdateClientText()
		{

			ClientNameLabel.Text = zlOrders.GetClient().name;
			ShippingAdressLabel.Text = zlOrders.GetShipping().shipping_adress;

		}


		public void UpdatePayTypeText()
		{

            PayTypeLabel.Text = PayType[zlOrders.pay_type];
		}
		public void UpdateStatusText()
		{

			StatusLabel.Text = StatusType[zlOrders.status];
		}



		public async void ShowDatePicker()
		{


			var modalPicker = new ModalPickerViewController(ModalPickerType.Date, "Select A Date", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.DatePicker.Mode = UIDatePickerMode.Date;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {
			   var dateFormatter = new NSDateFormatter()
			   {
				   DateFormat = "YYYY-MM-dd"
			   };

			   zlOrders.date = dateFormatter.ToString(modalPicker.DatePicker.Date);


			   DateLabel.Text = zlOrders.GetDate();
		   };

			await PresentViewControllerAsync(modalPicker, true);

		}


		public async void ShowClientPicker()
		{

			ClientPickerModel model = new ClientPickerModel(zlClients);
			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Client", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {


			   int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);
			   zlOrders.SetClient(zlClients[temp]);
			   zlOrders.SetShippings(zlOrders.GetClient().GetShippingAdress()[0]);
			   UpdateClientText();


		   };
			await PresentViewControllerAsync(modalPicker, true);



		}
		public async void ShowShippingPicker()
		{

			ShippingPickerModel model = new ShippingPickerModel(zlOrders.GetClient().GetShippingAdress());


			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Shipping", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {


			   int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);

			   zl_shipping zl = zlOrders.GetClient().GetShippingAdress()[temp];

			   zlOrders.SetShippings(zl);
			   UpdateClientText();

		   };
			await PresentViewControllerAsync(modalPicker, true);




		}
		public async void ShowPayTypePicker()
		{

			SimplePickerModel model = new SimplePickerModel(PayType);

			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select PayType", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {

			   int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);
                zlOrders.pay_type = temp;
			   UpdatePayTypeText();



		   };
			await PresentViewControllerAsync(modalPicker, true);



		}


		public async void ShowStatusPicker()
		{

			SimplePickerModel model = new SimplePickerModel(StatusType);

			var modalPicker = new ModalPickerViewController(ModalPickerType.Custom, "Select Status", this)
			{
				HeaderBackgroundColor = UIColor.FromRGB(253, 201, 9),
				HeaderTextColor = UIColor.White,
				TransitioningDelegate = new ModalPickerTransitionDelegate(),
				ModalPresentationStyle = UIModalPresentationStyle.Custom
			};
			modalPicker.PickerView.Model = model;


			modalPicker.OnModalPickerDismissed += (s, ea) =>
		   {
			   int temp = (int)modalPicker.PickerView.SelectedRowInComponent(0);
			   zlOrders.status = temp;
			   UpdateStatusText();



		   };
			await PresentViewControllerAsync(modalPicker, true);







		}


		CGRect PickerFrameWithSize(CGSize size)
		{
			var screenRect = UIScreen.MainScreen.ApplicationFrame;
			return new CGRect(0f, screenRect.Height - size.Height, size.Width, size.Height);
		}



		public void AddOrderDetail()
		{

			if (zlDepozit.Count == 0)
				return;
			zl_orders_detail zl = new zl_orders_detail();
			zl.SetDepozit(zlDepozit[0]);


			zlOrders.GetOrdersDetail().Add(
				zl

			);

			TableView.ReloadData();
			TableView.ScrollToRow(NSIndexPath.FromRowSection(zlOrders.GetOrdersDetail().Count - 1, 0), UITableViewScrollPosition.Bottom, true);
		}

		public void ShowAlert(string title, string message, params string[] buttons)
		{

			var alert = new UIAlertView
			{
				Title = title,
				Message = message
			};
			foreach (var button in buttons)
				alert.AddButton(button);
			alert.Clicked += (s, e) => alert.Dispose();
			alert.Show();

		}

		public virtual bool ShouldReturn(UITextField textField)
		{

			DescriptionLabel.ResignFirstResponder();
			return true;
		}

        partial void BackButton_TouchUpInside(UIButton sender)
		{


			GoToPanel();
		}



		public void GoToPanel() { 
		
		
			var controller = this.Storyboard.InstantiateViewController("panelControllerid") as panelViewController;

			if (controller != null)
			{


				this.NavigationController.PushViewController(controller, true);
			}
		
		}

		async partial  void SendButton_TouchUpInside(UIButton sender)
		{

			var bounds = UIScreen.MainScreen.Bounds;

			// show the loading overlay on the UI thread using the correct orientation sizing
			LoadingOverlay loadPop = new LoadingOverlay(bounds, "Logining...");
			View.Add(loadPop);
			//zlOrders.zlOrderDetail = GetCorrectlyDetail();
			if (CheckOrder())
			{

					
				HttpResponseModel response = await request.SendOrders(zlOrders,1);
				if (response.Succes)
				{
					var controller = this.Storyboard.InstantiateViewController("panelControllerid") as panelViewController;
					switch (response.Ok)
					{
						
						case 0:

							var temp = JObject.Parse(response.Response);
							int units = Int32.Parse(temp["units"].ToString());
							int index = Int32.Parse(temp["index"].ToString());


							ShowAlert("Error", "Ati introdus mai multe unitati decat se afla in depozit" + "(" + units + ")", new string[] { "ok" });
							TableView.ScrollToRow(NSIndexPath.FromRowSection(index, 0), UITableViewScrollPosition.Bottom, true);

							break;



						case 1:


							zl_orders models = JsonConvert.DeserializeObject<zl_orders>(response.Response);


							Console.WriteLine("ADDD order  id " + models.id + " respone  " + response + " json from  obj " + models.GetJson());




							db.ReplaceOrder(models);

							db.ReplaceOrdersDetail(models.zlOrderDetail,models.id);




							List<zl_depozit> depozitsList = new List<zl_depozit>();
							for (int i = 0; i < zlOrders.GetOrdersDetail().Count; i++)
							{


								zl_depozit depozit = zlOrders.GetOrdersDetail()[i].GetDepozit();

								depozit.number_units -= zlOrders.GetOrdersDetail()[i].units;

								depozitsList.Add(depozit);


							}
							db.ReplaceDepozit(depozitsList);

							if (controller != null)
							{
								this.NavigationController.PushViewController(controller, true);
							}

							break;
						case 2:

							ShowAlert("Error", "Aceasta comanda nu mai exista ea va disparea dupa urmatoarea actualizarea aplicatie", new string[] { "ok" });



							if (controller != null)
							{
								this.NavigationController.PushViewController(controller, true);
							}

							break;

					}

				}
				else
				{
					UIAlertView alert = new UIAlertView()
					{
						Title = "Error",
						Message = "Nu exista conexiune la internet"
					};
					alert.AddButton("OK");

					alert.Show();
				}
			}

			loadPop.Hide();
		}
	}
}