// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace md.onecode.app.litra
{
    [Register ("EditOrderDetailViewController")]
    partial class EditOrderDetailViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView OrderDetailTableView { get; set; }

        [Action ("UIButton402_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void UIButton402_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (OrderDetailTableView != null) {
                OrderDetailTableView.Dispose ();
                OrderDetailTableView = null;
            }
        }
    }
}