// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace md.onecode.app.litra
{
    [Register ("panelViewController")]
    partial class panelViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton AllOrderButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AnimatedButtons.LiquidFloatingActionButton fab { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton MyOrderButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView OrderTableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        AnimatedButtons.LiquidFloatingActionButton SearchFab { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView statusImage0 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView statusImage1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView statusImage2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel statusLabel0 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel statusLabel1 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel statusLabel2 { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton update { get; set; }

        [Action ("UIButton272_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void UIButton272_TouchUpInside (UIKit.UIButton sender);

        [Action ("Update_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Update_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (AllOrderButton != null) {
                AllOrderButton.Dispose ();
                AllOrderButton = null;
            }

            if (fab != null) {
                fab.Dispose ();
                fab = null;
            }

            if (MyOrderButton != null) {
                MyOrderButton.Dispose ();
                MyOrderButton = null;
            }

            if (OrderTableView != null) {
                OrderTableView.Dispose ();
                OrderTableView = null;
            }

            if (SearchFab != null) {
                SearchFab.Dispose ();
                SearchFab = null;
            }

            if (statusImage0 != null) {
                statusImage0.Dispose ();
                statusImage0 = null;
            }

            if (statusImage1 != null) {
                statusImage1.Dispose ();
                statusImage1 = null;
            }

            if (statusImage2 != null) {
                statusImage2.Dispose ();
                statusImage2 = null;
            }

            if (statusLabel0 != null) {
                statusLabel0.Dispose ();
                statusLabel0 = null;
            }

            if (statusLabel1 != null) {
                statusLabel1.Dispose ();
                statusLabel1 = null;
            }

            if (statusLabel2 != null) {
                statusLabel2.Dispose ();
                statusLabel2 = null;
            }

            if (update != null) {
                update.Dispose ();
                update = null;
            }
        }
    }
}