﻿using System;
using System.Collections.Generic;
using SQLite;
using Newtonsoft.Json;
namespace md.onecode.app.litra
{
	public class zl_orders
	{   
		[JsonProperty]
		[AutoIncrement, PrimaryKey]
		public int id { get; set; }
		[JsonProperty]
		public int id_client { get; set; }
		[JsonProperty]
		public int status { get; set; }
		[JsonProperty]
        public int pay_type{ get; set; }
		[JsonProperty]
		public string description { get; set; }
		[JsonProperty]
		public string date { get; set; }
		[JsonProperty]
		public int id_shipping { get; set; }

		public List<zl_orders_detail> zlOrderDetail=new List<zl_orders_detail>();
		zl_clients zlClients;
		zl_shipping zlShipping;

		public zl_clients GetClient() { return zlClients;}
		public zl_shipping GetShipping() { return zlShipping;}


		public void SetClient(zl_clients zlClients) { this.zlClients = zlClients; id_client = zlClients.id; }
		public void SetShippings(zl_shipping zlShipping) { this.zlShipping = zlShipping; id_shipping = zlShipping.id;}



		public void SetOrdersDetail(List<zl_orders_detail> zlOrdersDetail) {


			this.zlOrderDetail = zlOrdersDetail; 

		
		
		}


		public List<zl_orders_detail> GetOrdersDetail() { return this.zlOrderDetail; }



		public String GetDate()
		{


			Console.WriteLine("Date "+date);
			string temp = date;
			char a = '-';
			string[] split = temp.Split(a);
			string ret = "";
			int pos = Int32.Parse(split[1]);
			switch (pos)
			{
				case 1: ret = "ian"; break;
				case 2: ret = "feb"; break;
				case 3: ret = "mar"; break;
				case 4: ret = "apr"; break;
				case 5: ret = "mai"; break;
				case 6: ret = "iun"; break;
				case 7: ret = "iul"; break;
				case 8: ret = "aug"; break;
				case 9: ret = "sep"; break;
				case 10: ret = "oct"; break;
				case 11: ret = "noi"; break;
				case 12: ret = "dec"; break;

			}
			return "" + split[2] + " " + ret + ". " + split[0];
		}


		public string GetPrice()
		{
			double price=0.0;
			for (int i = 0; i < zlOrderDetail.Count; i++)
			{
				if (zlOrderDetail[i].new_price == 0)
				{
					price += zlOrderDetail[i].GetDepozit().price * zlOrderDetail[i].units;
				}
				else {
					price += zlOrderDetail[i].new_price * zlOrderDetail[i].units;
				}


			}

		//	Console.WriteLine("Price : " + price+" Detail size :"+zlOrderDetail.Count);


			return price.ToString();

		}



		public String GetPayType() {

            switch (pay_type) {


                case 0 : return "Tranfer"; break;
				case 1 : return "Cash"; break; 
                case 2 : return "Cec"; break;    
			
			}

			return "";
		
		
		}
	

		public String GetStatusType()
		{

			switch (status)
			{


				case 0: return "Inregistrat"; break;
				case 1: return "Livrat"; break;
				case 2: return "Achitat"; break;

			}

			return "";


		}


		public string GetJson() {


			List<zl_orders> zl = new List<zl_orders>();
			zl.Add(this);



            string json=JsonConvert.SerializeObject(zl).Replace("pay_type", "payType");


            Console.WriteLine("suka "+json);

            return json;


		}


	}






}
