﻿using System;
using SQLite;
namespace md.onecode.app.litra
{
	public class zl_depozit
	{
		[AutoIncrement, PrimaryKey]
		public int id { get; set; }
		public int id_product { get; set; }
		public string volum { get; set; }
		//public string filtered { get; set; }
		//public string type { get; set; }
		public int price { get; set; }
		public string expiration_date { get; set; }
		public int number_units { get; set; }
		public string description { get; set; }


		zl_product zlProduct;


		public void SetProduct(zl_product zl) {


			this.zlProduct = zl;
			id_product = zl.id;
		
		}
		public zl_product GetProduct() {


			return zlProduct;


		}



        public string GetVolume(){
          string volume="";

            switch (Int32.Parse(volum)) {
                
            case 0: volume="0.33l"; break;
            case 1: volume="0.5l"; break;
            case 2: volume="0.75l"; break;
            case 3: volume="1l"; break;
            case 4: volume="1.5l"; break;
            case 5: volume="2l"; break;
            case 6: volume="3l"; break;
            case 7: volume="20l"; break;
            case 8: volume="30l"; break;
            case 9: volume="50l"; break;

        }

        return volume;
       }




	
	}
}
