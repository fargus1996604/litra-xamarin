﻿using System;
namespace md.onecode.app.litra
{
	public class HttpResponseModel
	{
		public bool Succes { get; set; }
		public int Ok { get; set; }
		public string Response { get; set; }
	}
}
