﻿using System;
using SQLite;
namespace md.onecode.app.litra
{
	public class zl_product
	{
		[AutoIncrement, PrimaryKey]
		public int id { get; set; }
		public string name { get; set; }
		public string description { get; set; }
		public string filtered { get; set; }
		public string type { get; set; }
	}
}
