﻿using System;
using System.Collections.Generic;
using SQLite;
namespace md.onecode.app.litra
{
	public class zl_clients
	{
		[AutoIncrement, PrimaryKey]
		public int id { get; set; }
		public string name { get; set; }
		public string updated_at { get; set; }
		public string juridic_adress { get; set; }
		public int id_users { get; set; }
		List<zl_shipping> _shippingAdress;



		public void SetShippingAdress(List<zl_shipping> zl) {

			_shippingAdress = zl;
		
		}



		public List<zl_shipping> GetShippingAdress() { return _shippingAdress; }


	}
}
