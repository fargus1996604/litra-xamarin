﻿using System;
using SQLite;
namespace md.onecode.app.litra
{
	public class zl_orders_detail
	{
		[AutoIncrement, PrimaryKey]
		public int id { get; set; }
		public int id_order { get; set; }
		public int id_deposit { get; set; }
		public double new_price { get; set; }
		public int units { get; set; }

		 zl_depozit zlDepozit;



		public void SetDepozit(zl_depozit zlDepozit) {



			this.zlDepozit = zlDepozit;

			id_deposit = zlDepozit.id;
		}

		public zl_depozit GetDepozit() { 
			

			return this.zlDepozit;}


	}
}
