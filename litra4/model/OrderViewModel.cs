﻿using System;
namespace md.onecode.app.litra
{
	public class OrderViewModel
	{

	    public string Name { get; set; }

		public DateTime StartDate { get; set; }

		public string Description { get; set; }

	}
}
