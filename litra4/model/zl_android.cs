﻿using System;
using SQLite;
namespace md.onecode.app.litra
{
	public class zl_android
	{
		[AutoIncrement, PrimaryKey]
		public int id { get; set; }
		public string table{ get; set; }
		public string date_hash{ get; set; }


		public zl_android() { }
        public zl_android(string table, string date_hash) {
			this.table = table;
            this.date_hash = date_hash;
		
		}


	}
}
